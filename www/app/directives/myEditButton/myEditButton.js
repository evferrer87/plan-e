app.directive('myEditButton', function($timeout) {
  return {
    restrict: 'A',
    scope: {
      updateFunction: '&updatefunct'
    },
    link: function(scope, element, attrs) {

      scope.isEditing = false;

      init();

      element[0].addEventListener('click', function() {
        changeIcon();
      });

      function init() {
        var currentElem = element[0];
        while (currentElem != null && !currentElem.classList.contains("item-with-edit-button")) {
          currentElem = currentElem.parentElement;
        }
        if (currentElem != null) {
          setDisable(currentElem);
        }
      }

      function changeIcon() {
        var currentElem = element[0];
        while (currentElem != null && !currentElem.classList.contains("item-with-edit-button")) {
          currentElem = currentElem.parentElement;
        }
        if (currentElem != null) {
          if (scope.isEditing) {
            setDisable(currentElem);
            scope.isEditing = false;
            scope.updateFunction();
          } else {
            setEnable(currentElem);
            scope.isEditing = true;
          }
        }
      }

      function setEnable(elem) {
        elem.classList.add("enable-input-edit");
        elem.classList.remove("disable-input-edit");
        activeInput(elem, false);
      }

      function setDisable(elem) {
        elem.classList.add("disable-input-edit");
        elem.classList.remove("enable-input-edit");
        activeInput(elem, true);
      }

      function activeInput(elem, value) {
        var inputs = elem.querySelectorAll("input");
        for (var i = 0; i < inputs.length; i++) {
          inputs[i].disabled = value;
        }
        var textAreas = elem.querySelectorAll("textarea");
        for (var i = 0; i < textAreas.length; i++) {
          textAreas[i].disabled = value;
        }
      }
    }
  };
})
