app.directive('locationSuggestion', function($ionicModal, GoogleMapsService, $rootScope, $timeout){
  return {
    restrict: 'A',
    scope: {
      location: '=?',
      mapOptions: '=',
      executeAfter: '&executeAfter'
    },
    link: function(scope, element, attrs){
      scope.search = {};
      scope.search.suggestions = [];
      scope.search.query = "";

      $ionicModal.fromTemplateUrl('app/directives/locationSuggestion/location.html', {
        scope: scope,
        focusFirstInput: true,
        animation: 'none'
      }).then(function(modal) {
        scope.modal = modal;
      });

      element[0].addEventListener('focus', function() {
        scope.open();
      });

      scope.open = function() {
        scope.modal.show();
      };

      scope.close = function() {
        scope.modal.hide().then(function() {
          scope.search.query = "";
        });
      };

      scope.searchAddress = function(newValue){
        GoogleMapsService.searchAddress(newValue).then(function(result) {
          scope.search.suggestions = result;
        }, function(status){
          scope.search.suggestions = { 0: {'description': newValue }};
        });
      }

      scope.$watch('search.query', function(newValue) {
        if(newValue) {
          scope.searchAddress(newValue);
        }else{
          if(newValue == ""){
            scope.search.suggestions = [];
          }
        }
      });

      scope.choosePlace = function(place) {
        if(!place.hasOwnProperty('place_id')){
          scope.defaultValue();
        }else{
          scope.updateDetail(place);
        }
        scope.close();
      };

      scope.updateDetail = function(place) {
        GoogleMapsService.getDetails(place.place_id).then(function (location) {
          scope.location.formatted_address = location.formatted_address;
          scope.location.placeid = place.place_id;

          GoogleMapsService.geocodePlaceId(scope.location.placeid).then(function(results) {
            scope.location.latitude = results[0].geometry.location.lat();
            scope.location.longitude = results[0].geometry.location.lng();
          })

          scope.executeAfter();
        }, function(status){
          scope.defaultValue();
        });
      }

      scope.defaultValue = function() {
        scope.location.formatted_address = scope.search.query;
        scope.location.placeid = null;
        scope.location.latitude = null;
        scope.location.longitude = null;
        scope.executeAfter();
        $rootScope.$broadcast('addressChanged');
      }
    }
  }
});
