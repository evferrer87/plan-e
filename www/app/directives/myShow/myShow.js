app.directive('myShow', function() {
  return {
    restrict: 'A',
    scope: {
      options: '=?',
      opposite: '=',
      display: '='
    },
    link: function(scope, element, attrs) {

      scope.verifyVisibility = function() {
        var result = 'visible';
        if (scope.options.show == false || scope.options.show == null || scope.options.show == '') {
          result = 'hidden';
        }

        if (scope.opposite) {
          if (result == 'visible') {
            result = 'hidden';
          } else {
            result = 'visible';
          }
        }
        return result;
      }

      var initialDisplay = element.css('display');
      scope.setElemVisibility = function(element, cssVisibility) {
        if (scope.display) {
          if (cssVisibility == 'hidden') {
            element.css('display','none');
          } else {
            element.css('display',initialDisplay);
          }
        } else {
          element.css('visibility',cssVisibility);
        }
      }

      var cssVisibility = scope.verifyVisibility();
      scope.setElemVisibility(element, cssVisibility);


      scope.$watch('options.show', function(newValue, oldValue){
        if (newValue != oldValue) {
          cssVisibility = scope.verifyVisibility();
          if (scope.options.show == false) {
            scope.setElemVisibility(element, cssVisibility);
          } else {
            if (scope.options.show == true) {
              scope.setElemVisibility(element, cssVisibility);
            }
          }
        }
      });


    }
  };
})
