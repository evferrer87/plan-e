app.directive('itemListDetail', function() {
  return {
    restrict: 'A',
    replace: true,
    transclude: true,
    link: function(scope, element, attrs) {

      scope.$parent.toggleDetail = function(event) {

        var parent = event.target.parentElement;

        if (parent.nodeName != 'DIV') {
          parent = parent.parentElement;
        }

        var e = parent.querySelectorAll(".item-detail")[0];

        if (e == 'undefined' || e == null) return;

        if(e.classList.contains("hide-item-detail")){
          scope.removeAllShowClass();
          e.classList.remove("hide-item-detail");
          e.classList.add("show-item-detail");

          var selects = e.querySelectorAll("select");
          for (var i = 0; i < selects.length; i++) {
            selects[i].addEventListener("focus", function(event) {
              console.log(event);
              event.stopPropagation();
              event.preventDefault();
              this.blur();
            })
          }
        }else{
          if(e.classList.contains("show-item-detail")) {
            e.classList.remove("show-item-detail");
            e.classList.add("hide-item-detail");
          }else{
            e.classList.add("hide-item-detail");
          }

          var editButtons = element[0].querySelectorAll('[my-edit-button]');
          for (var i = 0; i < editButtons.length; i++) {

          }
        }
      }

      scope.removeAllShowClass = function() {
        var elems = element[0].querySelectorAll(".show-item-detail");
        for (var i = 0; i < elems.length; i++) {
          elems[i].classList.remove("show-item-detail");
          elems[i].classList.add("hide-item-detail");
        }
      }

    }
  };
})
