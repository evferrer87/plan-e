app.directive("customEmail", function() {
  return {
    require: "ngModel",
    link: function(scope, element, attributes, ngModel) {

      ngModel.$validators.customEmail = function(modelValue) {
        return checkEmail(modelValue);
      };
    }
  };
});
