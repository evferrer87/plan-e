app.directive("equalValue", function() {
  return {
    require: "ngModel",
    scope: {
      obj: "=equalValue"
    },
    link: function(scope, element, attributes, ngModel) {

      ngModel.$validators.equalValue = function(modelValue) {
        for (var i = 0; i < scope.obj.list.length; i++) {
          if (scope.obj.list[i][scope.obj.field] == modelValue
            && (scope.obj.list[i].id != scope.obj.elemID) ) {
            return false;
          }
        }
        return true;
      };
    }
  };
});
