app.directive('myNavBar', function() {
  return {
    restrict: 'E',
    replace: true,
    template: '<div class="my-nav-bar"></div>'
  };
})

app.directive('myNavButtons', function() {
  return {
    restrict: 'E',
    link: function(scope, element, attrs) {

      var navbar = document.querySelectorAll(".my-nav-bar")[0];
      while (navbar.firstChild) {
        navbar.removeChild(navbar.firstChild);
      }

      navbar.appendChild(element[0]);
    }
  };
})
