app.directive('iconSwitcher', function() {

  return {
    restrict : 'A',

    link : function(scope, elem, attrs) {

      var currentState = true;

      elem.on('click', function() {
        console.log('You clicked me!');

        var target = elem;
        if (elem[0].nodeName == "BUTTON") {
          target = elem[0].getElementsByTagName("I")[0];
        }

        if(currentState === true) {
          angular.element(target).removeClass(attrs.onIcon);
          angular.element(target).addClass(attrs.offIcon);
        } else {
          console.log('It is off!');
          angular.element(target).removeClass(attrs.offIcon);
          angular.element(target).addClass(attrs.onIcon);
        }

        currentState = !currentState

      });
    }
  };
});
