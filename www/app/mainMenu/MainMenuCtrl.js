app.controller('MainMenuCtrl', function($scope, $rootScope, $ionicPopover, $state){

  $scope.settings = function(){
    $scope.closeMainMenu();
    $state.go('settings');
  }

  $scope.exit = function(){
    ionic.Platform.exitApp();
  }

  $scope.closeMainMenu = function() {
    $scope.popoverMainMenu.hide();
  }
});
