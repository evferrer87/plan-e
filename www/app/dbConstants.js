app.constant('DB_CONFIG', {
  name: 'DB',
  version: '1.0',
  description: 'My database',
  size: 5 * 1024 * 1024,
  backProcesing: 0,
  location: 'default',

  tables: {
    events: {
      columns: {
        id: 'integer primary key autoincrement',
        name: 'text unique',
        budget: 'numeric',
        startdate: 'numeric',
        starttime: 'numeric',
        enddate: 'numeric',
        endtime: 'numeric',
        detail: 'text',
        image: 'text',
        state: 'integer'
      }
    },
    locations: {
      columns: {
        id: 'integer primary key autoincrement',
        placeid: 'text',
        formatted_address: 'text',
        map_zoom: 'integer',
        event_id: 'integer not null unique',
        latitude: 'integer',
        longitude: 'integer'
      },
      constraints: {
        0: 'foreign key(event_id) references events(id) on delete cascade'
      }
    },
    tasks: {
      columns: {
        id: 'integer primary key autoincrement',
        name: 'text',
        startdate: 'numeric',
        starttime: 'numeric',
        enddate: 'numeric',
        endtime: 'numeric',
        completed: 'integer',
        detail: 'numeric',
        event_id: 'integer not null'
      },
      constraints: {
        0: 'foreign key(event_id) references events(id) on delete cascade'
      },
      indexes: {
        0: 'unique index IF NOT EXISTS indexTasks_name_event_Unique on tasks (name,event_id)'
      }
    },
    guests: {
      columns: {
        id: 'integer primary key autoincrement',
        event_id: 'numeric',
        name: 'text',
        phone: 'text',
        email: 'text',
        notified: 'integer'
      },
      constraints: {
        0: 'foreign key(event_id) references events(id) on delete cascade'
      },
      indexes: {
        0: 'unique index IF NOT EXISTS indexGuests_name_event_Unique on guests (name,event_id)'
      }
    },
    expenses: {
      columns: {
        id: 'integer primary key autoincrement',
        event_id: 'numeric',
        concept: 'text',
        value: 'numeric',
        detail: 'text'
      },
      constraints: {
        0: 'foreign key(event_id) references events(id) on delete cascade'
      },
      indexes: {
        0: 'unique index IF NOT EXISTS indexExpenses_concept_event_Unique on expenses (concept,event_id)'
      }
    }
  }
});
