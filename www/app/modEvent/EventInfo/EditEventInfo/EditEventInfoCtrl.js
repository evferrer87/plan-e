eventModule.controller("EditEventInfoCtrl", function(
  $scope, $ionicModal, EventsService, $rootScope, $timeout, ObjUtils, ImageService, EventSet) {

  $scope.events = EventSet.get();

  $scope.changeEventInfo = function() {
    if (!angular.equals($scope.editEvent, $scope.event)) {
      $scope.saveButtonOptions.show = true;
    }else{
      $scope.saveButtonOptions.show = false;
    }
    $scope.verifyIfEventImageExist();
  }

  $scope.changeEventBudget = function() {
    ObjUtils.parseFloat($scope.editEvent, 'budget');
    $scope.changeEventInfo();
  }

  $scope.saveEvent = function(editEventForm) {
    if (!editEventForm.$valid) return;
    var equalLocation = false;
    if (angular.equals($scope.editEvent.location, $scope.event.location)) {
      equalLocation = true;
    }
    EventsService.saveEvent($scope.editEvent).then(function() {
      $scope.saveButtonOptions.show = false;
      if (!equalLocation) {
        $rootScope.$broadcast('mapContainerUpdate', 'map1', $scope.editEvent.location);
      }
      $scope.closeEditEventModal();
    });
  }

  $scope.addImage = function(event) {
    event.preventDefault();
    event.stopPropagation();
    ImageService.loadImageDialog(event).then(function(imgUrl) {
      $scope.editEvent.image = imgUrl;
      $scope.changeEventInfo();
    });
  }

  $scope.removeImage = function(event) {
    event.preventDefault();
    event.stopPropagation();
    $scope.editEvent.image = null;
    $scope.changeEventInfo();
  }

});
