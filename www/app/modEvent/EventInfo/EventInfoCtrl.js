eventModule.controller('EventInfoCtrl', function($scope, EventsService, $stateParams, $q, $rootScope, $ionicModal){

  $scope.saveButtonOptions = {show: false};

  $scope.saveEvent = function(event) {
    EventsService.saveEvent(event).then(function(){
    });
  }

  $scope.openEditEventInfoModal = function() {
    $scope.saveButtonOptions = {show: false};
    $scope.editEvent = angular.copy($scope.event);
    $scope.buttonImage = {show: false};

    $scope.verifyIfEventImageExist = function() {
      if ($scope.editEvent.image == null || $scope.editEvent.image == '') {
        $scope.buttonImage.show = false;
      } else {
        $scope.buttonImage.show = true;
      }
    }

    $scope.verifyIfEventImageExist();

    $ionicModal.fromTemplateUrl('app/modEvent/EventInfo/EditEventInfo/editEventInfo.html', {
      scope: $scope,
      focusFirstInput: false,
      animation: 'slide-in-up',
      backdropClickToClose: false
    }).then(function(modal) {
      $scope.editEventInfoModal = modal;
      $scope.editEventInfoModal.show();
    });
  };

  $scope.closeEditEventModal = function() {
    $scope.editEventInfoModal.hide().then(function() {
      $scope.editEventInfoModal.remove();
    });
  };

  $scope.openLocationMap = function (e, event) {
    e.preventDefault();
    e.stopPropagation();
    $ionicModal.fromTemplateUrl('app/modEvent/EventInfo/LocationMap/locationMap.html', {
      scope: $scope,
      focusFirstInput: false,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.locationMapModal = modal;
      $scope.editLocation = angular.copy(event.location);
      $scope.locationMapModal.show().then(function () {
        $rootScope.$broadcast('mapContainerReady');
      })
    });
  };

  $scope.closeLocationMapModal = function () {
    $scope.locationMapModal.hide().then(function () {
      $scope.locationMapModal.remove();
    });
  };

  $scope.$on('$destroy', function() {
    if ($scope.editEventInfoModal) {
      $scope.editEventInfoModal.remove();
    }
    if ($scope.locationMapModal) {
      $scope.locationMapModal.remove();
    }
    //Remove all map listener
    $rootScope.mapContainerReadyListener();
    $rootScope.mapContainerUpdateListener();
  });
});
