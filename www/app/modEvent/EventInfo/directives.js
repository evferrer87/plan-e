eventModule.directive('locationMap', function(GoogleMapsService, $timeout, $rootScope) {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      location: '=?',
      movable: '=?',
      id: '@'
    },
    link: function(scope, element, attrs) {

      $rootScope.mapContainerReadyListener = $rootScope.$on('mapContainerReady', function() {
        scope.mapDiv = element[0];
        $timeout(function(){
          this.paintMap(scope.mapDiv, scope.location);
        }, 400);
      });

      $rootScope.mapContainerUpdateListener = $rootScope.$on('mapContainerUpdate', function(event, idMap, location) {
        $timeout(function() {
          scope.mapDiv = document.getElementById(idMap);
          if (scope.mapDiv) {
            this.paintMap(scope.mapDiv, location);
          }
        });
      });

      this.paintMap = function(mapDiv, location) {
        var movable = false;
        if (scope.mapDiv.hasAttribute('movable')) {
          if (scope.mapDiv.getAttribute('movable') == "true") {
            movable = true;
          };
        }
        var map = new google.maps.Map(mapDiv, {
          zoom: location.map_zoom,
          draggable: movable,
          scrollwheel: movable,
          disableDoubleClickZoom: movable,
          zoomControl: movable
        });

        var infowindow = new google.maps.InfoWindow();

        GoogleMapsService.geocodePlaceId(location.placeid).then(function(results) {
          //console.log(movable);
          if (results[0]) {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: map,
              position: results[0].geometry.location,
              draggable: movable,
              label: {
                text: 'E',
                color: 'black'
              },
              icon: {
                path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
                fillColor: '#68FFA5',
                fillOpacity: 1,
                strokeColor: '#000',
                strokeWeight: 0.5,
                scale: 1,
                labelOrigin: new google.maps.Point(0,-29)
              }
            });

            google.maps.event.addListener(marker, 'click', function() {
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map,marker);
            });

            map.addListener('zoom_changed', function(evt) {
              location.map_zoom = map.getZoom();
              $timeout(function() {
                $rootScope.$broadcast('map_changed');
              });
            });

            marker.addListener('dragend', function(evt){
              GoogleMapsService.geocodeLocation(evt.latLng.lat(), evt.latLng.lng()).then(function(results){
                location.placeid = results[1].place_id;
                location.formatted_address = results[1].formatted_address;
                location.latitude = results[1].geometry.location.lat();
                location.longitude = results[1].geometry.location.lng();
                infowindow.setContent(results[1].formatted_address);
                $timeout(function() {
                  $rootScope.$broadcast('map_changed');
                });
              },function(status){
                console.log("no connection");
              })
            });
          } else {
            console.log('No results found');
          }
        }, function(raeson){
          console.log("location not found");
        })
      };
    },
    template: '<div class="location-map"></div>'
  };
});
