eventModule.controller('LocationMapCtrl', function($scope, $ionicModal, $rootScope){

  $scope.saveLocationButton = {show: false};

  $scope.saveLocation = function() {
    $scope.editEvent.location = $scope.editLocation;
    $scope.saveButtonOptions.show = true;
    $scope.closeLocationMapModal();
  }

  $rootScope.$on('map_changed', function(){
    $scope.saveLocationButton.show = true;
    if(!angular.equals($scope.editLocation, $scope.editEvent.location)){
      $scope.saveLocationButton.show = true;
    }else{
      $scope.saveLocationButton.show = false;
    }
  })

});
