eventModule.service('EventSetLoader', function($q, $rootScope, EventDAO, EventSet){
  this.load = function() {
    var deferred = $q.defer();
    EventDAO.getAll().then(function (results) {
      EventSet.init(results);
      deferred.resolve();
    }, function(error) {
      deferred.reject(error);
    });
    return deferred.promise;
  }
});

eventModule.service('EventDAO', function($q, DateConverter, DTOAssembler, DB, EventDTO, LocationDTO) {

  this.getAll = function() {
    var deferred = $q.defer();
    var sql =
      "SELECT events.*, locations.id as location_id, locations.placeid, locations.formatted_address, " +
      "locations.map_zoom, locations.latitude, locations.longitude," +
      "(SELECT count(*) FROM tasks WHERE events.id = tasks.event_id ) as tasks_amount, " +
      "(SELECT count(*) FROM tasks WHERE events.id = tasks.event_id AND tasks.completed = 'true') as tasks_completed, " +
      "(SELECT count(*) FROM guests WHERE events.id = guests.event_id ) as guests_amount " +
      "FROM events LEFT OUTER JOIN locations on events.id = locations.event_id " +
      "ORDER BY events.startdate ASC";

    DB.query(this.sqlUpdateStatesEvents()).then(function (result) {
      console.log(result);
      DB.query(sql).then(function(data) {
        var result = [];
        var rows = data.rows;
        for (var i = 0; i < rows.length; i++) {
          var row = rows.item(i);
          var eventDTO = DTOAssembler.toDTO(row,"EventDTO");
          if (row.location_id == null) {
            eventDTO.location = LocationDTO.init();
          } else {
            eventDTO.location = {
              id: row.location_id,
              placeid: row.placeid,
              formatted_address: row.formatted_address,
              map_zoom: row.map_zoom,
              latitude: row.latitude,
              longitude: row.longitude
            }
          }
          eventDTO.startdate = DateConverter.toDateFromTimestamp(row.startdate);
          eventDTO.starttime = DateConverter.toDateFromTimestamp(row.starttime);
          eventDTO.enddate = DateConverter.toDateFromTimestamp(row.enddate);
          eventDTO.endtime = DateConverter.toDateFromTimestamp(row.endtime);
          result.push(eventDTO);
        }
        deferred.resolve(result);
      }, function(error) {
        deferred.reject(error);
      });
    })

    return deferred.promise;
  };

  this.insert = function(eventDTO) {
    var event = DTOAssembler.toModel(eventDTO, 'Event');
    event.startdate = event.startdate == null ? null : event.startdate.toISOString();
    event.enddate = event.enddate == null ? null : event.enddate.toISOString();
    event.starttime = event.starttime == null ? null : event.starttime.toISOString();
    event.endtime = event.endtime == null ? null : event.endtime.toISOString();
    console.log(event);
    var deferred = $q.defer();
    var entityObjs = DB.buildEntityObjs('events', event, true);
    DB.db.transaction(function(tx) {
      var insertEventSQL = DB.buildInsertSQL('events', entityObjs['events']);
      tx.executeSql(insertEventSQL.sql, insertEventSQL.values,function(transaction,result) {
        entityObjs['locations'].event_id = result.insertId;
        var insertLocationSQL = DB.buildInsertSQL('locations', entityObjs['locations']);
        tx.executeSql(insertLocationSQL.sql, insertLocationSQL.values, function(transaction, res) {
          deferred.resolve(result.insertId);
        });
      })
    }, function (error) {
      deferred.reject(error);
    });

    return deferred.promise;
  };

  this.remove = function(id) {
    var deferred = $q.defer();
    DB.remove('events', id).then(function() {
      deferred.resolve();
    }, function(error){
      deferred.reject(error);
    })

    return deferred.promise;
  };

  this.update = function(eventDTO) {
    var event = DTOAssembler.toModel(eventDTO, 'Event');
    event.startdate = event.startdate == null ? null : event.startdate.toISOString();
    event.enddate = event.enddate == null ? null : event.enddate.toISOString();
    event.starttime = event.starttime == null ? null : event.starttime.toISOString();
    event.endtime = event.endtime == null ? null : event.endtime.toISOString();
    var deferred = $q.defer();
    var entityObjs = DB.buildEntityObjs('events', event, true);
    entityObjs['locations'].event_id = event.id;
    DB.db.transaction(function(tx) {
      var updateEventSQL = DB.buildUpdateQuery('events', entityObjs['events']);
      tx.executeSql(updateEventSQL.sql, updateEventSQL.values,function(transaction,result) {
        var upateLocationSQL = DB.buildUpdateQuery('locations', entityObjs['locations']);
        tx.executeSql(upateLocationSQL.sql, upateLocationSQL.values, function(transaction, res) {
          deferred.resolve();
        });
      })
    }, function (error) {
      console.log(error);
      deferred.reject(error);
    });

    return deferred.promise;
  }

  this.sqlUpdateStatesEvents = function() {
    return "" +
      "UPDATE events SET state = " +
      "CASE WHEN enddate < date('now') then 0 " +
      "ELSE 1 END";
  }

});
