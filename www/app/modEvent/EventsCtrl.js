eventModule.controller('EventsCtrl', function($scope, EventsService, $ionicPopover, $ionicModal, EventDTO, $rootScope,$state){

  $scope.events = [];
  $scope.expiredEvents = false;

  $scope.loadEvents = function() {
    $scope.expiredEvents = false;
    EventsService.getEvents().then(function (results) {
      for (var i = 0; i < results.length; i++) {
        if (results[i].state == 0) $scope.expiredEvents = true;
      }
      $scope.events = results;
    });
  }

  $scope.$on('$ionicView.afterEnter', function() {
    $scope.loadEvents();
  });

  $scope.openMainMenu = function($event) {
    $ionicPopover.fromTemplateUrl('app/mainMenu/mainMenu.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popoverMainMenu = popover;
      $scope.popoverMainMenu.show($event);
    });
  };

  $scope.openAddEventModal = function() {
    $scope.saveButtonOptions = {show: false};
    $scope.newEvent = EventDTO.init();
    $scope.editEvent = angular.copy($scope.newEvent);
    $scope.buttonImage = {show: false};

    $scope.verifyIfEventImageExist = function() {
      if ($scope.editEvent.image == null || $scope.editEvent.image == '') {
        $scope.buttonImage.show = false;
      } else {
        $scope.buttonImage.show = true;
      }
    }

    $ionicModal.fromTemplateUrl('app/modEvent/AddEvent/addEvent.html', {
      scope: $scope,
      controller: 'AddEventCtrl',
      focusFirstInput: false,
      animation: 'slide-in-up',
    }).then(function(modal) {
      $scope.addEventModal = modal;
      $scope.addEventModal.show();
    });
  };

  $scope.closeEditEventModal = function() {
    $scope.addEventModal.hide().then(function() {
      $scope.addEventModal.remove();
    });
  };

  $scope.openLocationMap = function (event) {
    $ionicModal.fromTemplateUrl('app/modEvent/EventInfo/LocationMap/locationMap.html', {
      scope: $scope,
      focusFirstInput: false,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.locationMapModal = modal;
      $scope.editLocation = angular.copy(event.location);
      $scope.locationMapModal.show().then(function () {
        $rootScope.$broadcast('mapContainerReady');
      })
    });
  };

  $scope.closeLocationMapModal = function () {
    $scope.locationMapModal.hide().then(function () {
      $scope.locationMapModal.remove();
    });
  };
});
