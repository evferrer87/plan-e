eventModule.service('EventsService', function($rootScope, $q, EventSetLoader, EventSet, EventDAO){

  this.getEvents = function() {
    var deferred = $q.defer();
    EventSetLoader.load().then(function() {
      deferred.resolve(EventSet.get());
    })
    return deferred.promise;
  }

  this.getEvent = function(id) {
    var deferred = $q.defer();
      var event = EventSet.search(id);
      if (event != null) {
        deferred.resolve(EventSet.search(id));
      }else {
        deferred.reject("event not found");
      }
    return deferred.promise;
  }

  this.createEvent = function(event) {
    var deferred = $q.defer();
    EventDAO.insert(event).then(function(id) {
      EventSetLoader.load().then(function() {
        deferred.resolve();
      })
    }, function(error) {
      console.log(error.message);
      deferred.reject(error);
    });
    return deferred.promise;
  };

  this.removeEvent = function(id) {
    EventDAO.remove(id).then(function() {
      EventSet.remove(id);
    }, function (error) {
      console.log(error);
    });
  }

  this.saveEvent = function(event) {
    var deferred = $q.defer();
    EventDAO.update(event).then(function(){
      var eventInSet = EventSet.search(event.id);
      if (!angular.equals(eventInSet, event)) {
        angular.copy(event, eventInSet);
      }
      angular.copy(event,EventSet.search(event.id));
      deferred.resolve();
    });
    return deferred.promise;
  }

  $rootScope.$on('addedTask', function(event, eventId){
    var eventDTO = EventSet.search(eventId);
    if (eventDTO) {
      eventDTO.task_amount++;
    }
  })

});
