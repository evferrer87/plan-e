eventModule.controller('EventListMenuCtrl', function($scope, EventsService,$state) {

  $scope.remove = function(){
    EventsService.removeEvent($scope.selectedEvent.id);
    $scope.popover.hide();
    $state.reload();
  }

})
