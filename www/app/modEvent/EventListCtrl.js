eventModule.controller('EventListCtrl', function($scope, $rootScope, $ionicPopover){
  $ionicPopover.fromTemplateUrl('app/modEvent/EventListMenu/eventListMenu.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.showSubmenu = function($event, event) {
    $scope.selectedEvent = event;
    $scope.popover.show($event);
  };
});
