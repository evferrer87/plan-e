eventModule.controller('AddEventCtrl', function($scope, $ionicPopup, $state, EventsService, ObjUtils, ImageService, $rootScope){

  $scope.changeEventInfo = function() {
    if (!angular.equals($scope.editEvent, $scope.newEvent)) {
      $scope.saveButtonOptions.show = true;
    }else{
      $scope.saveButtonOptions.show = false;
    }
    $scope.verifyIfEventImageExist();
  }

  $scope.changeEventBudget = function() {
    ObjUtils.parseFloat($scope.editEvent, 'budget');
    $scope.changeEventInfo();
  }

  $scope.saveEvent = function(editEventForm) {
    if (!editEventForm.$valid) return;
    EventsService.createEvent($scope.editEvent).then(function() {
      $scope.loadEvents();
      $scope.saveButtonOptions.show = false;
      $scope.closeEditEventModal();
    });
  }

  $scope.addImage = function(event) {
    event.stopPropagation();
    event.preventDefault();
    ImageService.loadImageDialog(event).then(function(imgUrl) {
      $scope.editEvent.image = imgUrl;
      $scope.changeEventInfo();
    });
  }

  $scope.removeImage = function(event) {
    event.preventDefault();
    event.stopPropagation();
    $scope.editEvent.image = null;
    $scope.changeEventInfo();
  }

});
