eventModule.controller('EventDetailCtrl', function($scope, $stateParams, EventsService, $q, LocationDTO, $rootScope, $timeout, $filter){
  $scope.getEvent = function(id) {
    var deferred = $q.defer();
    EventsService.getEvent(id).then(function(event) {
      $scope.event = event;
      if(event.location == null){
        $scope.event.location = LocationDTO.init();
      }

      $timeout(function(){
        $rootScope.$broadcast('mapContainerReady');
      })

      deferred.resolve($scope.event);
    });
    return deferred.promise;
  }

  $scope.getEvent($stateParams.eventId);
});
