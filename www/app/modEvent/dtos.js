setMakerFn(eventModule, 'EventSet');    //Register Singleton Generic Collection Factory

eventModule.factory("EventDTO", function(LocationDTO) {
  function EventDTO(id, name, budget, startdate, starttime, enddate, endtime, location, detail, image, state, tasks_amount, tasks_completed, guests_amount) {
    this.id = id;
    this.name = name;
    this.budget = budget;
    this.startdate = startdate;
    this.starttime = starttime;
    this.enddate = enddate;
    this.endtime = endtime;
    this.location = location;
    this.detail = detail;
    this.image = image;
    this.state = state;
    this.tasks_amount = tasks_amount;
    this.tasks_completed = tasks_completed;
    this.guests_amount = guests_amount;
  };
  EventDTO.build = function(data) {
    return new EventDTO(
      data.id,
      data.name,
      data.budget,
      data.startdate,
      data.starttime,
      data.enddate,
      data.endtime,
      LocationDTO.build(data.location),
      data.detail,
      data.image,
      data.state,
      data.tasks_amount,
      data.tasks_completed,
      data.guests_amount
    );
  };

  EventDTO.prototype.getDates = function() {
    return {
      'startdate': this.startdate,
      'starttime': this.starttime,
      'enddate': this.enddate,
      'endtime': this.endtime
    }
  }
  EventDTO.init = function() {
    return new EventDTO(null, undefined, null, null, null, null, null, LocationDTO.init(), "", "", 1, 0, 0, 0);
  };
  return EventDTO;
});

eventModule.factory("LocationDTO", function() {
  function LocationDTO(id, placeid, formatted_address, map_zoom, latitude, longitude) {
    this.id = id;
    this.placeid = placeid;
    this.formatted_address = formatted_address;
    this.map_zoom = map_zoom;
    this.latitude = latitude;
    this.longitude = longitude;

  };
  LocationDTO.build = function(data) {
    return data == null ? null : new LocationDTO(
      data.id,
      data.placeid,
      data.formatted_address,
      data.map_zoom,
      data.latitude,
      data.longitude
    );
  };
  LocationDTO.init = function() {
    return new LocationDTO(null,"","",17,"","");
  };
  return LocationDTO;
});
