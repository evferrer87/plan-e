eventModule.factory("Event", function(LocationDTO) {
  function Event(id, name, budget, startdate, starttime, enddate, endtime, location, detail, image, state) {
    this.id = id;
    this.name = name;
    this.budget = budget;
    this.startdate = startdate;
    this.starttime = starttime;
    this.enddate = enddate;
    this.endtime = endtime;
    this.location = location;
    this.detail = detail;
    this.image = image;
    this.state = state;
  };
  Event.build = function(data) {
    return new Event(
      data.id,
      data.name,
      data.budget,
      data.startdate,
      data.starttime,
      data.enddate,
      data.endtime,
      LocationDTO.build(data.location),
      data.detail,
      data.image,
      data.state
    );
  };
  Event.init = function() {
    return new Event(null, undefined, null, null, null, null, null, Location.init(), "", "", 1);
  };
  return Event;
});

app.factory("Location", function() {
  function Location(id, placeid, formatted_address, map_zoom, latitude, longitude) {
    this.id = id;
    this.placeid = placeid;
    this.formatted_address = formatted_address;
    this.map_zoom = map_zoom;
    this.latitude = latitude;
    this.longitude = longitude;
  };
  Location.build = function (data) {
    return data == null ? null : new Location(
      data.id,
      data.placeid,
      data.formatted_address,
      data.map_zoom,
      data.latitude,
      data.longitude
    );
  };
  Location.init = function () {
    return new Location(null, "", "", 17, "", "");
  };
  return Location;
})

app.factory("Task", function() {
  function Task(id, name, startdate, starttime, enddate, endtime, detail, completed) {
    this.id = id;
    this.name = name;
    this.startdate = startdate;
    this.starttime = starttime;
    this.enddate = enddate;
    this.endtime = endtime;
    this.detail = detail;
    this.completed = completed;
  };
  Task.build = function(data) {
    return new Task(
      data.id,
      data.name,
      data.startdate,
      data.starttime,
      data.enddate,
      data.endtime,
      data.detail,
      data.completed
    );
  };
  Task.init = function() {
    return new Task(null,undefined,null,null,null,null, "", false);
  };
  return Task;
})

app.factory("Guest", function() {
  function Guest(id, name, phone, email, notified) {
    this.id = id;
    this.name = name;
    this.phone = phone;
    this.email = email;
    this.notified = notified;
  };
  Guest.build = function(data) {
    return new Guest(
      data.id,
      data.name,
      data.phone,
      data.email,
      data.notified
    );
  };
  Guest.init = function() {
    return new Guest(null,"", "", "",false);
  };
  return Guest;
})

app.factory("Expense", function() {
  function Expense(id, concept, value, detail) {
    this.id = id;
    this.concept = concept;
    this.value = value;
    this.detail = detail;
  };
  Expense.build = function(data) {
    return new Expense(
      data.id,
      data.concept,
      data.value,
      data.detail
    );
  };
  Expense.init = function() {
    return new Expense(null,undefined, null, "");
  };
  return Expense;
})
