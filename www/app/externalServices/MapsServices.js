app.service('GoogleMapsService', function($q){
  var self = this;

  self.searchAddress = function(input) {
    var deferred = $q.defer();

    try {
      var autocompleteService = new google.maps.places.AutocompleteService();
      autocompleteService.getPlacePredictions({
        input: input
      }, function(result, status) {
        if(status == google.maps.places.PlacesServiceStatus.OK){
          deferred.resolve(result);
        }else{
          deferred.reject(status);
        }
      }, function(status){
        deferred.reject(status);
      });
    }catch (ex) {
      deferred.reject(ex);
    }

    return deferred.promise;
  };

  self.getDetails = function(placeId) {
    var deferred = $q.defer();
    var detailsService = new google.maps.places.PlacesService(document.createElement("input"));
    detailsService.getDetails({placeId: placeId}, function(place, status) {
      if(status == google.maps.places.PlacesServiceStatus.OK){
        deferred.resolve(place);
      }else{
        deferred.reject(status);
      }
    }, function(raeson) {
      deferred.reject(raeson);
    });
    return deferred.promise;
  };

  self.geocodePlaceId = function(placeId) {
    var deferred = $q.defer();
    var geocoderService = new google.maps.Geocoder();
    geocoderService.geocode({'placeId': placeId}, function(results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        deferred.resolve(results);  //results[0]
      } else {
        deferred.reject(status);
      }
    });
    return deferred.promise;
  };

  self.geocodeLocation = function(latitude, longitude) {
    var deferred = $q.defer();
    var geocoderService = new google.maps.Geocoder();
    var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
    geocoderService.geocode({'location': latlng}, function(results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        deferred.resolve(results);  //results[1]
      } else {
        deferred.reject(status);
      }
    });
    return deferred.promise;
  };

  return self;
});
