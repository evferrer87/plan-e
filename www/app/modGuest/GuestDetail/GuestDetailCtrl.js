guestModule.controller('GuestDetailCtrl', function($scope, EmailService, GuestsService, $ionicPopup) {

  $scope.notifyGuest  = function() {
    EmailService.notifyGuests($scope.event, [$scope.guest]).then(function (message) {
      var confirmNotification = $ionicPopup.confirm({
        title: 'Confirmación',
        template: '¿Desea marcar este participante como notificado?'
      });

      confirmNotification.then(function (res) {
        if (res) {
          $scope.guest.notified = true;
          GuestsService.saveGuest($scope.guest);
        }
      });
    });
  }

});
