guestModule.factory('ContactSetLoader', function($q, $cordovaContacts, ContactSet, $http){
  var self = this;
  self.loadFromPhone = function() {
    var deferred = $q.defer();

    ContactSet.removeAll();

    var opts = {
      filter: " ",
      multiple: true,
      fields: ['displayName', 'name']
    };

    $cordovaContacts.find(opts).then(function (contactsFound) {
     for (var i = 0; i < contactsFound.length; i++) {
      ContactSet.add(contactsFound[i]);
     }
     deferred.resolve();
     }, function(error) {
      deferred.reject(error);
    });
    return deferred.promise;
  }

  self.LoadFromJsonFile = function(){
    var deferred = $q.defer();
    $http.get('data/contactsMock.json').then(function(res){
      ContactSet.removeAll();
      for (var i = 0; i < res.data.length; i++) {
        ContactSet.add(res.data[i]);
      }
      //ContactSet.init(res.data);
      deferred.resolve();
    }, function(error) {
      console.error("File not loaded");
    });
    return deferred.promise;
  }

  return self;
});

guestModule.service('GuestDAO', function($q, DB, DTOAssembler) {
  this.getAll = function(eventId) {
    var deferred = $q.defer();
    var sql = "SELECT * FROM guests WHERE guests.event_id = ?";
    DB.query(sql, [eventId]).then(function(data){
      var result = [];
      var rows = data.rows;
      for (var i = 0; i < rows.length; i++) {
        var row = rows.item(i);
        row.notified == "true" ? row.notified = true : row.notified = false;  //ORM return "true" or "false" strings
        var guestDTO = DTOAssembler.toDTO(row, 'GuestDTO');
        result.push(guestDTO);
      }
      deferred.resolve(result);
    }, function(error){
      deferred.reject(error);
    });
    return deferred.promise;
  }

  this.insert = function(eventId, guest) {
    var deferred = $q.defer();
    guest.event_id = eventId;
    DB.insert('guests', guest).then(function(result) {
      deferred.resolve(result.insertId);
    }, function(error){
      deferred.reject({message: error});
    });
    return deferred.promise;
  }

  this.update = function(guestDTO) {
    var deferred = $q.defer();
    var guest = DTOAssembler.toModel(guestDTO, 'Guest');
    DB.update('guests', guest).then(function(result) {
      deferred.resolve(result);
    }, function(error) {
      deferred.reject(error);
    });

    return deferred.promise;
  }

  this.remove = function(id) {
    var deferred = $q.defer();
    DB.remove('guests', id).then(function() {
      deferred.resolve();
    }, function(error){
      deferred.reject(error);
    });
    return deferred.promise;
  }

  this.insertAll = function(eventId, guests) {
    var deferred = $q.defer();
    for (var i = 0; i < guests.length; i++) {
      guests[i].event_id = eventId;
    }
    DB.insertAll('guests', guests, true).then(function(result) {
      var guestsDTO = [];
      for (var i = 0; i < result.length; i++) {
        guestsDTO.push(DTOAssembler.toModel(result[i], 'Guest'));
      }
      deferred.resolve(guestsDTO);
    }, function(error){
      deferred.reject({message: error});
    });
    return deferred.promise;
  }

  this.updateAll = function(eventId, guests, isTx) {
    var deferred = $q.defer();
    for (var i = 0; i < guests.length; i++) {
      guests[i].event_id = eventId;
    }
    DB.updateAll('guests', guests, isTx).then(function(result) {
      var guestsDTO = [];
      for (var i = 0; i < result.length; i++) {
        guestsDTO.push(DTOAssembler.toModel(result[i], 'Guest'));
      }
      deferred.resolve(guestsDTO);
    }, function(error){
      deferred.reject({message: error});
    });
    return deferred.promise;
  }

  this.getSearchByName = function(eventId, guestsName) {
    var deferred = $q.defer();
    var inClause = "";
    if (guestsName.length != null && guestsName.length != undefined && guestsName.length > 0) {
      inClause = "and name in (" + guestsName.toString() + ")";
    }
    var sql = "SELECT * FROM guests WHERE guests.event_id = ?" + inClause;
    DB.query(sql, [eventId]).then(function(data){
      var result = [];
      var rows = data.rows;
      for (var i = 0; i < rows.length; i++) {
        var row = rows.item(i);
        var guestDTO = DTOAssembler.toDTO(row, 'GuestDTO');
        result.push(guestDTO);
      }
      deferred.resolve(result);
    }, function(error){
      deferred.reject(error);
    });
    return deferred.promise;
  }

  this.insertAndUpdateAll = function(eventId, guests, isTx) {
    var deferred = $q.defer();
    for (var i = 0; i < guests.length; i++) {
      if (guests[i].id == null) {
        guests[i].event_id = eventId;
      } else {
        if (guests[i].hasOwnProperty('event_id')) {
          delete guests[i].event_id;
        }
      }
    }
    DB.insertAndUpdateAll('guests', guests, isTx).then(function(data){
      deferred.resolve(guests);
    }, function(error){
      deferred.reject(error);
    });
    return deferred.promise;
  }
});
