guestModule.controller('GuestsCtrl', function($scope, $stateParams, GuestsService, $ionicModal, SelectHelper, GuestDTO, $ionicPopover){
  GuestsService.getGuests($stateParams.eventId).then(function(guests) {
    $scope.guestArray = new SelectHelper.SelectionArray(guests);
    console.log($scope.guestArray.get());
  });

  $scope.openAddGuestModal = function() {
    $scope.newGuest = GuestDTO.init();
    $scope.editGuest = angular.copy($scope.newGuest);
    function openGuestmodal() {
      $ionicModal.fromTemplateUrl('app/modGuest/AddGuest/addGuest.html', {
        scope: $scope,
        focusFirstInput: false,
        animation: 'slide-in-up',
        hardwareBackButtonClose: false
      }).then(function(modal) {
        $scope.addGuestModal = modal;
        $scope.addGuestModal.show();
      });
    }

    openGuestmodal();
  }

  $scope.closeAddGuestModal = function() {
    $scope.addGuestModal.remove();
  };

  $scope.$on('$destroy', function() {
    if ($scope.addGuestModal) {
      $scope.addGuestModal.remove();
    }
  });

  $scope.openSearchGuestModal = function() {
    $ionicModal.fromTemplateUrl('app/modGuest/SearchGuest/searchGuest.html', {
      scope: $scope,
      focusFirstInput: false,
      animation: 'none'
    }).then(function(modal) {
      $scope.searchGuestModal = modal;
      $scope.searchGuestModal.show();
    });
  }

  $scope.openGuestMenu = function($event) {
    $ionicPopover.fromTemplateUrl('app/modGuest/GuestMenu/guestMenu.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popoverGuestMenu = popover;
      $scope.popoverGuestMenu.show($event);
    });
  };

});
