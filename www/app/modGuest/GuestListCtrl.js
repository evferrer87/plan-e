guestModule.controller('GuestListCtrl', function($scope, $stateParams, GuestsService, $ionicModal, $ionicPopover) {

  $scope.openGuestDetail = function (guest) {
    $scope.guest = guest;
    $ionicModal.fromTemplateUrl('app/modGuest/GuestDetail/guestDetail.html', {
      scope: $scope,
      focusFirstInput: false,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.guestDetailmodal = modal;
      $scope.guestDetailmodal.show();
    });
  };

  $scope.closeGuestDetail = function () {
    $scope.guestDetailmodal.hide().then(function() {
      $scope.guestDetailmodal.remove();
    });
  };

  $scope.call = function () {
    alert("implementar");
  }

  $scope.showSubmenu = function($event, guest) {
    $ionicPopover.fromTemplateUrl('app/modGuest/GuestListMenu/guestListMenu.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.guest = guest;
      $scope.popover = popover;
      $scope.popover.show($event);
    });
  };

  $scope.openEditGuestModal = function() {
    $ionicModal.fromTemplateUrl('app/modGuest/EditGuest/editGuest.html', {
      scope: $scope,
      focusFirstInput: false,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.editGuestModal = modal;
      $scope.editGuestModal.show();
    });
  };

  $scope.closeEditGuestModal = function() {
    $scope.editGuestModal.hide().then(function() {
      $scope.editGuestModal.remove();
    });
  };

  $scope.$on('$destroy', function () {
    if ($scope.guestDetailmodal) {
      $scope.guestDetailmodal.remove();
    }
    if ($scope.editGuestModal) {
      $scope.editGuestModal.remove();
    }
  });

  $scope.guestSelectChanged = function(event, guest) {
    event.stopPropagation();
  }
})
