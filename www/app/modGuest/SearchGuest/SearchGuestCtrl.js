guestModule.controller("SearchGuestCtrl", function($scope, $ionicModal) {

  $scope.$on('$destroy', function() {
    $scope.searchGuestModal.remove();
  });

  $scope.close = function() {
    $scope.searchGuestModal.hide();
  };

});
