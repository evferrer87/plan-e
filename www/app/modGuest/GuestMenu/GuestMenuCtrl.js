guestModule.controller('GuestMenuCtrl', function($scope, EmailService, $ionicPopup, GuestsService, ObjUtils){

  $scope.closeGuestMenu = function() {
    $scope.popoverGuestMenu.hide();
  }

  $scope.notifyGuests  = function() {
    if (!$scope.guestArray.anySelected()) {
      $scope.closeGuestMenu();
      $scope.showAlert('Selección', 'Debes seleccionar al menos un participante para notificar.');
    } else {
      $scope.closeGuestMenu();
      EmailService.notifyGuests($scope.event, $scope.guestArray.getAllSelected()).then(function(message) {
        var confirmNotification = $ionicPopup.confirm({
          title: 'Confirmación',
          template: '¿Desea marcar los participantes seleccionados como notificados?'
        });

        confirmNotification.then(function(res) {
          if (res) {
            var notifiedGuests = $scope.guestArray.getAllSelected();
            for (var i = 0; i < notifiedGuests.length; i++) {
              notifiedGuests[i].notified = true;
            }
            GuestsService.updateAll(notifiedGuests);
          } else {

          }
        });
      }, function(error) {
        $scope.showAlert('Error', error);
      });
    }
  }

  $scope.selectAllGuests = function() {
    $scope.guestArray.selectAll(true);
    $scope.closeGuestMenu();
  }

  $scope.unselectAllGuests = function() {
    $scope.guestArray.selectAll(false);
    $scope.closeGuestMenu();
  }

  $scope.markGuestsAsNotified = function() {
    if ($scope.verifySelectedGuests()) {
      ObjUtils.updateArrayProperty($scope.guestArray.getAllSelected(),'notified',true);
      GuestsService.updateAll($scope.guestArray.getAllSelected());
    }
    $scope.closeGuestMenu();
  }

  $scope.markGuestsAsNotNotified = function() {
    if ($scope.verifySelectedGuests()) {
      ObjUtils.updateArrayProperty($scope.guestArray.getAllSelected(),'notified',false);
      GuestsService.updateAll($scope.guestArray.getAllSelected());
    }
    $scope.closeGuestMenu();
  }

  $scope.verifySelectedGuests = function() {
    if (!$scope.guestArray.anySelected()) {
      $scope.closeGuestMenu();
      $scope.showAlert('Selección', 'Debes seleccionar al menos un participante para notificar.');
      return false;
    }
    return true;
  }

  $scope.showAlert = function(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      buttons: [
        {text: 'Aceptar', type:'button-positive'},
      ]
    });
    alertPopup.then(function (res) {
      $scope.closeGuestMenu();
    });
  }

});
