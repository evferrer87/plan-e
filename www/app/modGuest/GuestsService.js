guestModule.service('GuestsService', function($q, GuestDAO, EventSet){

  this.getGuests = function(eventId) {
    var deferred = $q.defer();
    GuestDAO.getAll(eventId).then(function(guests){
      EventSet.search(eventId).guests_amount = guests.length;
      deferred.resolve(guests);
    },function(error) {
      deferred.reject(error);
    });
    return deferred.promise;
  }

  this.addGuest = function(eventId, guest) {
    var deferred = $q.defer();
    GuestDAO.insert(eventId, guest).then(function(id) {
      guest.id = id;
      deferred.resolve();
    }, function(error) {
      deferred.reject(error);
    });
    return deferred.promise;
  }

  this.saveGuest = function(guest) {
    var deferred = $q.defer();
    GuestDAO.update(guest).then(function() {
      deferred.resolve();
    }, function(error) {
      deferred.reject(error);
    });
    return deferred.promise;
  }

  this.removeGuest = function(id) {
    var deferred = $q.defer();
    GuestDAO.remove(id).then(function(){
      deferred.resolve();
    },function(error){
      console.log(error);
    });
    return deferred.promise;
  }

  this.addMany = function(eventId, guests, isTx) {
    var deferred = $q.defer();
    if (guests != null && guests != 'undefined') {
      GuestDAO.insertAll(eventId, guests, isTx).then(function(result) {
        deferred.resolve(result);
      }, function(error){
        console.error(error);
        deferred.reject(error);
      })
    }
    return deferred.promise;
  }

  this.overwriteGuests = function(eventId, guests, isTx) {
    var deferred = $q.defer();
    if (guests != null && guests != 'undefined') {
      GuestDAO.insertAndUpdateAll(eventId, guests, isTx).then(function (result) {
        deferred.resolve(guests);
      }, function (error) {
        console.error(error);
        deferred.reject(error);
      })
    } else {
      deferred.reject("Null values passed");
    }

    return deferred.promise;
  };

  this.updateAll = function(guests) {
    if (guests == null || guests == 'undefined') {
      return;
    }

    for (var i = 0; i < guests.length; i++ ){
      this.saveGuest(guests[i]);
    }
  }
});
