setMakerFn(guestModule, 'ContactSet'); //Register Singleton Generic Collection Factory

guestModule.factory("GuestDTO", function() {
  function GuestDTO(id, name, phone, email, notified) {
    this.id = id;
    this.name = name;
    this.phone = phone;
    this.email = email;
    this.notified = notified;
  };

  GuestDTO.build = function(data) {
    return new GuestDTO(
      data.id,
      data.name,
      data.phone,
      data.email,
      data.notified
    );
  };

  GuestDTO.init = function() {
    return new GuestDTO(null,"", "", "", false);
  };
  return GuestDTO;
});
