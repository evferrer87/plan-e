guestModule.controller("EditGuestCtrl", function($scope, $ionicModal, GuestsService) {

  $scope.saveButtonOptions = {show: false};
  $scope.editGuest = angular.copy($scope.guest);

  $scope.changeGuest = function() {
    if (!angular.equals($scope.editGuest, $scope.guest)) {
      $scope.saveButtonOptions.show = true;
    }else{
      $scope.saveButtonOptions.show = false;
    }
  }

  $scope.saveGuest = function(editGuestForm) {
    if (!editGuestForm.$valid) return;
    GuestsService.saveGuest($scope.editGuest).then(function() {
      $scope.saveButtonOptions.show = false;
      angular.copy($scope.editGuest,$scope.guestArray.search($scope.editGuest.id));
      $scope.closeEditGuestModal();
    });
  }

});
