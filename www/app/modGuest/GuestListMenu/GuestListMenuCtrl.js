guestModule.controller('GuestListMenuCtrl', function($scope, GuestsService) {
  $scope.remove = function(){
    GuestsService.removeGuest($scope.guest.id).then(function() {
      $scope.guestArray.remove($scope.guest.id);
      $scope.event.guests_amount--;   //To update date in home view
      $scope.popover.hide();
    });
  }

  $scope.edit = function() {
    $scope.popover.hide();
    $scope.openEditGuestModal();
  }

})
