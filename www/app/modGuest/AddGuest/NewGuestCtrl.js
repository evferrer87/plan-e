guestModule.controller('NewGuestCtrl', function($scope, GuestsService){

  $scope.addNuewGuest = function(addNewGuestForm) {
    if (!addNewGuestForm.$valid) return;
    GuestsService.addGuest($scope.event.id, $scope.newGuest).then(function(){
      $scope.guestArray.add($scope.newGuest);
      $scope.event.guests_amount++;
      $scope.closeAddGuestModal();
    });
  }

})
