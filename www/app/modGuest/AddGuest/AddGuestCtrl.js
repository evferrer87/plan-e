guestModule.controller('AddGuestCtrl', function($scope, $ionicModal, Guest){

  $scope.saveButtonOptions = {show: false};

  $scope.originOptions = [
    {id:0, value: 'Nuevo', url: 'app/modGuest/AddGuest/newGuest.html'},
    {id:1, value: 'Teléfono', url: 'app/modGuest/AddGuest/phoneContacts.html'}
  ];

  $scope.selectedOrigin = $scope.originOptions[0];
  $scope.originView = $scope.originOptions[0].url;

  $scope.selectOriginView = function(selectedOrigin) {
    $scope.originView = selectedOrigin.url;
  }

  $scope.changeGuest = function() {
    if (!angular.equals($scope.newGuest, $scope.editGuest)) {
      $scope.saveButtonOptions.show = true;
    }else{
      $scope.saveButtonOptions.show = false;
    }
  }
});
