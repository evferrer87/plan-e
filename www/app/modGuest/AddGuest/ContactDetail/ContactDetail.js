guestModule.controller('ContactDetail', function($scope) {

  $scope.closeContactDetail = function() {
    $scope.contactDetailModal.hide();
  }

  $scope.$on('$destroy', function () {
    $scope.contactDetailModal.remove();
  })

  $scope.selectContactFromDetail = function(event, contact) {
    $scope.contactSelectChanged(event, contact);
    $scope.contactDetailModal.hide();
  }
})
