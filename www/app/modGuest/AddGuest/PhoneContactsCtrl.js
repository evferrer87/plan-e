guestModule.controller('PhoneContactsCtrl',
  function($scope, ContactsService, SelectHelper, $timeout, $ionicModal, ObjUtils, $ionicPopup, GuestDTO, GuestsService) {

  $scope.importButtonOptions = {show: true};
  $scope.allSelected = false;

  $scope.allContacts = new SelectHelper.SelectionArray([]);
  $scope.contacts = [];
  $scope.selectedAmount = 0;

  ContactsService.load().then(function() {
    $scope.allContacts = new SelectHelper.SelectionArray(ContactsService.getAll());

    for (var i = 0; i < $scope.allContacts.get().length; i++) {
      var emails = $scope.allContacts.get()[i]['emails'];
      var emailValue = null;
      if (emails != null) {
        emailValue = emails[0].value;
      }
      ObjUtils.addAttribute($scope.allContacts.get()[i], 'selectedEmail', emailValue);

      var phoneNumbers = $scope.allContacts.get()[i]['phoneNumbers'];
      var phoneNumberValue = null;
      if (phoneNumbers != null) {
        phoneNumberValue = phoneNumbers[0].value;
      }
      ObjUtils.addAttribute($scope.allContacts.get()[i], 'selectedPhoneNumber', phoneNumberValue);

      console.log($scope.allContacts.get()[i]);
      $scope.allContacts.get()[i]['nameExistInGuestList'] = false;
      for (var j = 0; j < $scope.guestArray.get().length; j++) {
        if (normalize($scope.guestArray.get()[j].name.toLowerCase()) == normalize($scope.allContacts.get()[i].displayName.toLowerCase())) {
          $scope.allContacts.get()[i]['nameExistInGuestList'] = true;
          $scope.allContacts.get()[i]['guestId'] = $scope.guestArray.get()[j].id;
          j = $scope.guestArray.get().length;
        }
      }
    }

    var contactsLoaded = 0;
    $scope.loadByAmount = function(amount) {
      $timeout(function(){
        for (var i = contactsLoaded; i < contactsLoaded + amount; i++) {
          $scope.contacts.push($scope.allContacts.get()[i]);
        }
        contactsLoaded = contactsLoaded + amount;
          $scope.showSpinner = false;
      })
    }

    $scope.loadMore = function() {
      $scope.loadByAmount($scope.allContacts.get().length);
      $scope.$broadcast('scroll.infiniteScrollComplete');
    };

    $scope.$on('$stateChangeSuccess', function() {
      $scope.loadMore();
    });

    $scope.loadMore();
  })


  $scope.contactSelectChanged = function(event, contact) {
    console.log(contact);
    event.stopPropagation();
    if ($scope.selectedAmount == $scope.allContacts.get().length) {
      if (contact.selected) {
        $scope.allSelected = true;
      } else {
        $scope.allSelected = false;
      }
    } else {
      if ($scope.selectedAmount + 1 == $scope.allContacts.get().length) {
        if (contact.selected) {
          $scope.allSelected = true;
        }
      }
    }
    if (contact.selected) {
      $scope.selectedAmount++;
    } else {
      $scope.selectedAmount--;
    }
  }

  $scope.selectAllContact = function() {
    $scope.allContacts.selectAll($scope.allSelected);
    if ($scope.allSelected) {
      $scope.selectedAmount = $scope.allContacts.get().length;
    } else {
      $scope.selectedAmount = 0;
    }
  }

  $scope.openContactDetail = function (contact) {
    $ionicModal.fromTemplateUrl('app/modGuest/AddGuest/ContactDetail/contactDetail.html', {
      scope: $scope,
      focusFirstInput: false,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.contactDetailModal = modal;
      $scope.contact = contact;
      console.log(contact);
      $scope.contactDetailModal.show();
    });
  }

  $scope.importContacts = function() {
    var guests = [];
    for (var i = 0; i < $scope.allContacts.get().length; i++) {
      var contact = $scope.allContacts.get()[i];
      if (contact.selected) {
        var newGuest = GuestDTO.init();
        newGuest.name = contact.displayName;
        if (contact.selectedEmail != null) {
          newGuest.email = contact.selectedEmail;
        }
        if (contact.selectedPhoneNumber != null) {
          newGuest.phone = contact.selectedPhoneNumber;
        }
        if (contact.nameExistInGuestList) {
          newGuest.id = contact.guestId;
        }
        if (!ObjUtils.arrayContainValue(guests, 'name', newGuest.name)) {
          guests.push(newGuest);
        }
      }
    }
    GuestsService.addMany($scope.event.id, guests, true).then(function(result) {
      GuestsService.getGuests($scope.event.id).then(function(guests) {
        $scope.guestArray.init(guests);
        $scope.showSuccesImportedAlert();
      });
    }, function(error) {
      $scope.showOverwriteContactsImportConfirm = function() {
        var confirmOverWritePopup = $ionicPopup.confirm({
          title: 'Confirmación',
          template: 'Algunos contactos seleccionados ya están en la lista de invitados. ¿Deseas sobreescribirlos?',
          buttons: [
            {text: 'Cancelar'},
            {
              text: 'Aceptar', type:'button-positive',
              onTap: function(e) {
                GuestsService.overwriteGuests($scope.event.id, guests, true).then(function(result) {
                  GuestsService.getGuests($scope.event.id).then(function(result) {
                    $scope.guestArray.init(result);
                    $scope.showSuccesImportedAlert();
                  });
                }, function(error) {
                  $scope.showErrorImportedAlert();
                });
              }
            },
          ]
        });
      }
      $scope.showOverwriteContactsImportConfirm();
    });
  }


  /**
   * Confirm Contacts Import
   */
  $scope.showContactsImportConfirm = function() {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Importar Contactos',
      template: '¿Deseas adicionar los contactos seleccionados?',
      buttons: [
        {text: 'Cancelar'},
        {
          text: 'Aceptar', type:'button-positive',
          onTap: function(e) {
            $scope.importContacts();
          }
        },
      ]
    });
  };

  $scope.addGuestFromContacts = function() {
    $scope.showContactsImportConfirm();
  }

  $scope.showSuccesImportedAlert = function () {
    var alertPopup = $ionicPopup.alert({
      title: 'Operación finalizada',
      template: '¡Contactos importados correctamente!',
      buttons: [
        {text: 'Aceptar', type:'button-positive'},
      ]
    });
    alertPopup.then(function (res) {
      $scope.closeAddGuestModal();
    });
  };

  $scope.showErrorImportedAlert = function () {
      var alertPopup = $ionicPopup.alert({
        title: 'Operación finalizada.',
        template: 'Importación fallida, intentelo nuevamente.',
        buttons: [
          {text: 'Aceptar', type:'button-positive'},
        ]
      });
      alertPopup.then(function (res) {
        $scope.closeAddGuestModal();
      });
    };
})
