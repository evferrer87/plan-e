expenseModule.controller('ExpenseListCtrl', function($scope, $rootScope, $ionicPopover, ExpensesService){
  $ionicPopover.fromTemplateUrl('app/modExpense/ExpenseListMenu/expenseListMenu.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.showSubmenu = function($event, expense) {
    $scope.expense = expense;
    $scope.popover.show($event);
  };
});
