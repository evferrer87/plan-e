expenseModule.controller("AddExpenseCtrl", function($scope, $ionicModal, ExpensesService, ExpenseDTO, ObjUtils) {

  $scope.saveButtonOptions = {show: false};
  $scope.expense = ExpenseDTO.init();
  $scope.editExpense = ExpenseDTO.init();

  $scope.changeExpense = function(){
    if(!angular.equals($scope.editExpense,$scope.expense)){
      $scope.saveButtonOptions.show = true;
    }else{
      $scope.saveButtonOptions.show = false;
    }
  }

  $scope.addExpense = function(addExpenseForm) {
    if (!addExpenseForm.$valid) return;
    ExpensesService.addExpense($scope.event.id, $scope.editExpense).then(function(){
      $scope.saveButtonOptions.show = false;
      $scope.updateExpensesView();
      $scope.closeAddExpenseModal();
    });
  }

  $scope.changeExpenseValue = function() {
    ObjUtils.parseFloat($scope.editExpense, 'value');
    $scope.changeExpense();
  }
});
