expenseModule.factory("ExpenseDTO", function() {
  function ExpenseDTO(id, concept, value, detail) {
    this.id = id;
    this.concept = concept;
    this.value = value;
    this.detail = detail;
  };
  ExpenseDTO.build = function(data) {
    return new ExpenseDTO(
      data.id,
      data.concept,
      data.value,
      data.detail
    );
  };
  ExpenseDTO.init = function() {
    return new ExpenseDTO(null,undefined, null, "");
  };
  return ExpenseDTO;
})
