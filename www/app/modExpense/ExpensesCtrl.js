expenseModule.controller('ExpensesCtrl', function($scope, ExpensesService, $stateParams, $ionicModal, SelectHelper, ObjUtils){

  var eventId = $stateParams.eventId;

  $scope.updateExpensesView = function() {
    ExpensesService.getExpenses(eventId).then(function(expenses) {
      $scope.expenseArray = new SelectHelper.SelectionArray(expenses);
    })
    $scope.updateExpensesViewValues();
  }

  $scope.updateExpensesViewValues = function() {
    ExpensesService.getExpensesSum(eventId).then(function(sum) {
      $scope.expensesSum = sum;
    });
  }

  $scope.updateExpensesView();

  $scope.updateExpense = function(expense) {
    ExpensesService.saveExpense(expense, eventId).then(function(){
      $scope.updateExpensesViewValues();
    });
  }

  $scope.closeEditExpenseModal = function() {
    $scope.editExpenseModal.hide().then(function() {
      $scope.editExpenseModal.remove();
    });
  };

  $scope.openAddExpenseModal = function() {
    $ionicModal.fromTemplateUrl('app/modExpense/AddExpense/addExpense.html', {
      scope: $scope,
      focusFirstInput: false,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.addExpenseModal = modal;
      $scope.addExpenseModal.show();
    });
  };

  $scope.closeAddExpenseModal = function() {
    $scope.addExpenseModal.hide();
  };

  $scope.$on('$destroy', function() {
    if ($scope.editExpenseModal) {
      $scope.editExpenseModal.remove();
    }

    if ($scope.addExpenseModal) {
      $scope.addExpenseModal.remove();
    }
  });

  $scope.changeExpenseValue = function(expense) {
    ObjUtils.parseFloat(expense, 'value');
  }

});
