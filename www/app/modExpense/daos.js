expenseModule.service('ExpenseDAO', function($q, DB, DateConverter, DTOAssembler) {
  this.getAll = function(eventId) {
    var deferred = $q.defer();
    var sql = "SELECT * FROM expenses WHERE expenses.event_id = ?";
    DB.query(sql, [eventId]).then(function(data){
      var result = [];
      var rows = data.rows;
      for (var i = 0; i < rows.length; i++) {
        var row =  rows.item(i);
        var expenseDTO = DTOAssembler.toDTO(row,"ExpenseDTO");
        result.push(expenseDTO);
      }
      deferred.resolve(result);
    }, function(error){
      deferred.reject(error);
    });
    return deferred.promise;
  }

  this.insert = function(eventId, expenseDTO) {
    var deferred = $q.defer();
    var expense = DTOAssembler.toModel(expenseDTO, 'Expense');
    expense.event_id = eventId;
    DB.insert('expenses', expense).then(function(result) {
      deferred.resolve(result.insertId);
    }, function(error){
      deferred.reject({message: error});
    });
    return deferred.promise;
  }

  this.update = function(expenseDTO, eventId) {
    var deferred = $q.defer();
    var expense = DTOAssembler.toModel(expenseDTO, 'Expense');
    DB.update('expenses', expense).then(function(result) {
      deferred.resolve(result);
    }, function(error) {
      deferred.reject(error);
    });

    return deferred.promise;
  }

  this.remove = function(id) {
    var deferred = $q.defer();
    DB.remove('expenses', id).then(function() {
      deferred.resolve();
    }, function(error){
      deferred.reject(error);
    });
    return deferred.promise;
  };

  this.getExpensesSum = function(eventId) {
    var deferred = $q.defer();
    var sql = "SELECT SUM(value) as sum FROM expenses WHERE expenses.event_id = ?";
    DB.query(sql, [eventId]).then(function(data){
      var row = data.rows.item(0);
      if (row['sum']) {
        deferred.resolve(row['sum']);
      } else {
        deferred.resolve(0);
      }

    }, function(error){
      deferred.reject(error);
    });
    return deferred.promise;
  }
});
