expenseModule.service('ExpensesService', function($q, ExpenseDAO){

  this.getExpenses = function(eventId) {
    var deferred = $q.defer();
    ExpenseDAO.getAll(eventId).then(function(expenses) {
      deferred.resolve(expenses);
    });
    return deferred.promise;
  }

  this.saveExpense = function(expense, eventId) {
    var deferred = $q.defer();
    ExpenseDAO.update(expense, eventId).then(function(){
      deferred.resolve();
    },function(error){
      console.log(error);
    });
    return deferred.promise;
  }

  this.removeExpense = function(id) {
    var deferred = $q.defer();
    ExpenseDAO.remove(id).then(function(){
      deferred.resolve();
    },function(error){
      console.log(error);
    });
    return deferred.promise;
  }

  this.addExpense = function(eventId, expense) {
    var deferred = $q.defer();
    ExpenseDAO.insert(eventId, expense).then(function(id){
      deferred.resolve(expense);
    },function(error){
      console.log(error);
    });
    return deferred.promise;
  }

  this.getExpensesSum = function(eventId) {
    var deferred = $q.defer();
    ExpenseDAO.getExpensesSum(eventId).then(function(sum){
      deferred.resolve(sum);
    },function(error){
      console.log(error);
    });
    return deferred.promise;
  }

});
