expenseModule.controller('ExpenseListMenuCtrl', function($scope, ExpensesService) {
  $scope.remove = function(){
    ExpensesService.removeExpense($scope.expense.id).then(function() {
      $scope.popover.hide();
      $scope.updateExpensesView();
    });
  }
});
