app.service('ContactsService', function(ContactSet, ContactSetLoader, $q) {

  this.load = function() {
    var deferred = $q.defer();
    ContactSetLoader.loadFromPhone().then(function(){
      console.log("Contacts loaded...");
      deferred.resolve();
    }, function(error) {
      console.error("Contacts not loaded");
      deferred.reject(error);
    });
    return deferred.promise;
  }

  this.getAll = function() {
    return ContactSet.get();
  }

  return this;
});
