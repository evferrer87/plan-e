app.factory('ImageService', function($cordovaCamera, $q, $rootScope, $ionicActionSheet) {

  var self = this;

  this.loadImageDialog = function(event) {
    var deferred = $q.defer();
    $rootScope.addImageSheet = $ionicActionSheet.show({
      buttons: [
        { text: 'Tomar foto' },
        { text: 'Elegir foto de la galería' }
      ],
      titleText: 'Adicionar imagen',
      cancelText: 'Cancel',
      cancel: function() {
        deferred.reject();
      },
      buttonClicked: function(index) {
        event.preventDefault();
        event.stopPropagation();
        loadImage(index).then(function(imageUrl){
          self.convertImgToDataURLviaCanvas(imageUrl,function(imgBase64) {
            deferred.resolve(imgBase64);
          });
          $rootScope.addImageSheet();
        }, function(error){
          deferred.reject();
        });
      }
    });
    return deferred.promise;
  }

  function optionsForType(type) {
    var source;
    switch (type) {
      case 0:
        source = Camera.PictureSourceType.CAMERA;
        break;
      case 1:
        source = Camera.PictureSourceType.PHOTOLIBRARY;
        break;
    }
    return {
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: source,
      allowEdit: false,
      encodingType: Camera.EncodingType.JPEG,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
  }

  function loadImage(type) {
    return $q(function(resolve, reject) {
      var options = optionsForType(type);
      $cordovaCamera.getPicture(options).then(function(imageUrl) {
        resolve(imageUrl);
      }, function(error) {
        reject(error);
      });
    })
  }

  this.convertImgToDataURLviaCanvas = function(url, callback, outputFormat){
    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function() {
      var canvas = document.createElement('CANVAS');
      var ctx = canvas.getContext('2d');
      var dataURL;
      canvas.width = 340;
      canvas.height = 307;
      ctx.drawImage(this, 0, 0, 340, 307);
      dataURL = canvas.toDataURL(outputFormat || 'image/png');
      callback(dataURL);
      canvas = null;
    };
    img.src = url;
  }

  return this;

});
