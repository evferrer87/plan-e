app.factory('EmailService', function($q, GoogleMapsService, ImageService, $filter) {

  this.notifyGuests = function(event, guests) {

    var deferred = $q.defer();

    function removeRepeatedEmails(emails) {
      var result = [];
      for (var i = 0; i < emails.length; i++) {
        if (result.indexOf(emails[i]) == -1) {
          result.push(emails[i]);
        }
      }
      return result;
    }

    function buildAttachments() {
      var deferred = $q.defer();
      var files = [];
      if (event.image) {
        //data:image/png;base64
        var img = event.image.replace(/^data.+base64,/i,'base64:evento.png//');
        files.push(img);
      }
      if (event.location.placeid) {
        ImageService.convertImgToDataURLviaCanvas(buildMap(), function(imgBase64){
          files.push(imgBase64.replace(/^data.+base64,/i,'base64:map.png//'));
          deferred.resolve(files);
        })
      } else {
        deferred.resolve(files);
      }
      return deferred.promise;
    }

    function buildBody() {
      return '' +
        '<p>Información sobre el evento:</p>' +
        '<h4>'+ event.name +'</h4>' +
        '<div style="float:left; margin-right:15px; margin-bottom:5px"></div>' +
        '<p>al que has sido invitado: </p>' +
        '<p>' + $filter('filterBuildInfoDate')(event.getDates()) +'</p>' +
        '<p>en: '+ event.location.formatted_address +'</p>' +
        '<p>'+ event.detail +'</p>' +
        '<div>¡Contamos con tu participación!</div>' +
        '<span>¡Muchas gracias!</span>';
    }

    function buildMap() {
      console.log(event.location);
      return 'http://maps.googleapis.com/maps/api/staticmap?center='+event.location.latitude+','+event.location.longitude+'&zoom='+
        17+'&scale=false&size=640x640&maptype=roadmap&format=png&visual_refresh=true" alt="'+
        event.location.formatted_address+'"&markers=color:0x68FFA5%7Clabel:E%7C'+event.location.latitude+','+
        event.location.longitude+'&key=AIzaSyAXZMf0SqWh7qVil6GEfMIMrixhqasv9Js';
    }

    var sendEmail = function() {
      var guestsCheckedEm = guestsCheckedEmails(guests);
      var emails = [];
      for (var i = 0; i < guestsCheckedEm.length; i++){
        emails.push(guestsCheckedEm[i].email);
      }

      var properties = {
        to:      removeRepeatedEmails(emails),
        subject: 'Notificación de invitación',
        body:    buildBody(),
        isHtml:  true
      };

      buildAttachments().then(function(files) {
        properties['attachments'] = files;
        cordova.plugins.email.open(properties, function () {
          console.log("Closed");
          deferred.resolve(guestsCheckedEm);
        });
      })
    }

    cordova.plugins.email.isAvailable(
      function (isAvailable) {
        if (!isAvailable) {
          console.error("Email composer not available.")
          deferred.reject("Debes asociar tu cuenta de correo en la aplicación de correos de tu teléfono");
        } else {
          sendEmail();
        }
      }
    );

    return deferred.promise;
  }

  return this;
});
