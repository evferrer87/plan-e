// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js

var taskModule = angular.module('modTask', []);
var guestModule = angular.module('modGuest', []);
var expenseModule = angular.module('modExpense', []);
var eventModule = angular.module('modEvent', ['modTask', 'modGuest', 'modExpense']);

var app = angular.module('plan-e', ['ionic', 'ngCordova',
  'modEvent', 'SqLite', 'ngMessages'
])

.run(function($ionicPlatform, $rootScope, $state, $ionicViewSwitcher, $ionicHistory, DB) {

  $ionicPlatform.ready(function() {

    if (window.cordova) {
      //Hide splashScreen
      setTimeout(function() {
        navigator.splashscreen.hide();
        console.log(navigator.splashscreen);
      });
    }

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
      DB.init();
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    $ionicPlatform.registerBackButtonAction(function(event) {
      var statesToHome = [
        "events.detail.info",
        "events.detail.tasks.list","events.detail.guests",
        "events.detail.expenses"
      ];
      var statesToExit = ["events.list"];
      var currentStateName = $state.current.name;
      if(statesToHome.indexOf(currentStateName) > -1){
        $rootScope.home();
      }else{
        if(statesToExit.indexOf(currentStateName) > -1) {
          ionic.Platform.exitApp();
        }else{
          $ionicHistory.goBack();
        }
      }
    }, 100);
    DB.init();
  });


  $rootScope.home = function() {
    $ionicViewSwitcher.nextDirection("back");
    $state.go("events.list");
    //$state.transitionTo('events.list', { reload: true, inherit: false, notify: true });
  };
})

app.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    .state('events',{
      abstract: true,
      url: '/events',
      template: '<ion-nav-view></ion-nav-view>'
    })

    .state('events.list',{
      url: '/eventsList',
      templateUrl: 'app/modEvent/events.list.html',
      controller: 'EventsCtrl',
      cache: false
    })

    .state('events.detail',{
      url: '/eventsDetail/:eventId',
      templateUrl: 'app/modEvent/EventDetail/event.detail.html',
      controller: 'EventDetailCtrl',
    })

    .state('events.detail.info',{
      url: '/eventInfo',
      views:{
        'tab-info':{
          templateUrl: 'app/modEvent/EventInfo/info.html',
          controller: 'EventInfoCtrl'
        }
      }
    })

    .state('events.detail.tasks', {
      url: '/tasks',
      abstract: true,
      views: {
        'tab-tasks': {
          template: "<ion-nav-view name='tab-tasks'></ion-nav-view>",
        }
      }
    })

    .state('events.detail.tasks.list',{
      url: '/tasksList',
      views:{
        'tab-tasks':{
          templateUrl: 'app/modTask/tasks.html',
          controller: 'TasksCtrl'
        }
      }
    })

    .state('events.detail.guests',{
      url: '/eventGuests',
      views:{
        'tab-guests':{
          templateUrl: 'app/modGuest/guests.html',
          controller: 'GuestsCtrl'
        }
      }
    })

    .state('events.detail.expenses',{
      url: '/eventExpenses',
      views:{
        'tab-accounting':{
          templateUrl: 'app/modExpense/expenses.html',
          controller: 'ExpensesCtrl'
        }
      }
    })

    .state('settings',{
      url: '/settings',
      templateUrl: 'app/settings/settings.html'
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('events/eventsList');
});
