app.service('DateConverter', function($filter){
  this.toDateFromTimestamp = function(value) {
    if (value != null &&  value != 0) {
      value = new Date($filter('date')(new Date(value), "yyyy/MM/dd HH:mm"));
    }
    return value;
  }

  this.toTimestampFromDate = function(value) {
    if (value != null && value != 0) {
      value = value.getTime();
    }
    return value;
  }
})

app.service('DateHelper', function($filter) {

  this.getMonths = function() {
    return new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
  }

  this.getWeekDays = function() {
    return new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
  }

  this.getYear = function(date) {
    return $filter('date')(date, "yyyy");
  }

  this.getMonthText = function(date) {
    return this.getMonths()[date.getMonth()];
  }

  this.getDay = function(date) {
    return $filter('date')(date, "dd");
  }

  return this;
})
