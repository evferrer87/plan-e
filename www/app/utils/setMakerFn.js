setMakerFn = function(module, identifier) {
  module.factory(identifier, function($filter) {
    var items = [];

    this.init = function (objs) {
      this.removeAll();
      items = objs;
    }

    this.getIndexById = function(id) {
      var index = -1;
      for (var i = 0; i < items.length; i++) {
        index++;
        if (items[i].id == id) {
          return index;
        }
      }
      return index;
    };

    this.add = function(item) {
      items.push(item);
    };

    this.get = function() {
      return items;
    };

    this.search = function(id) {
      return $filter('findById')(items, id);
    };

    this.remove = function(id) {
      var index = this.getIndexById(id);
      if (index != -1) {
        items.splice(index, 1);
      }
    }

    this.removeAll = function() {
      items.splice(0,items.length);
    }

    return this;
  });
};
