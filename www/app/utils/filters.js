app.filter('findById', function() {
  return function(objArray, id) {
    for (var i = 0; i < objArray.length; i++) {
      if (objArray[i].id == id) {
        return objArray[i];
      }
    }
    return null;
  };
});


app.filter('myDate', function($filter)
{
  return function(input)
  {
    if(input == null || input == 0){ return ""; }
    var date = $filter('date')(new Date(input));
    return date;
  };
});

app.filter('myShortDate', function($filter)
{
  return function(input)
  {
    if(input == null || input == 0){ return ""; }
    var date = $filter('date')(new Date(input), "dd/MM/yyyy");
    return date;
  };
});

app.filter('myTime', function($filter)
{
  return function(input)
  {
    if(input == null || input == 0){ return ""; }
    var time = $filter('date')(new Date(input), "HH:mm");
    return time;
  };
});

app.filter('filterByLimit', function($filter) {
  return function(objArray, value) {
    var result = $filter('filter')(objArray, value);
    if (value.displayName && value.displayName.length > 0) {
      return $filter('limitTo')(result, 10);
    }
    return result;
  };
});

app.filter('filterBuildInfoDate', function($filter, DateHelper) {

  codeDates = function(dates) {
    var code = "";
    code += (dates.hasOwnProperty("startdate") && (dates.startdate != null && dates.startdate != 'undefined')) ? 1 : 0;
    code += (dates.hasOwnProperty("starttime") && (dates.starttime != null && dates.starttime != 'undefined')) ? 1 : 0;
    code += (dates.hasOwnProperty("enddate") && (dates.enddate != null && dates.enddate != 'undefined')) ? 1 : 0;
    code += (dates.hasOwnProperty("endtime") && (dates.endtime != null && dates.endtime != 'undefined')) ? 1 : 0;
    return code;
  }

  codeTwoDates = function(date1, date2) {
    var code = "";
    if (date1 != null && date1 != 'undefined' && date2 != null && date2 != 'undefined') {
      code += (DateHelper.getDay(date1) == DateHelper.getDay(date2)) ? 1 : 0;
      code += (date1.getMonth() == date2.getMonth()) ? 1 : 0;
      code += (date1.getYear() == date2.getYear()) ? 1 : 0;
    } else {
      console.error("No se admiten fechas null");
    }
    return code;
  }

  buildDates = function(dates, code) {
    var result = "";

    switch (code) {
      case "1000":
        result = "El " + DateHelper.getDay(dates.startdate) +" de " + DateHelper.getMonthText(dates.startdate) + " de " +
          DateHelper.getYear(dates.startdate)
        break;
      case "1100":
        result = "El " + DateHelper.getDay(dates.startdate) +" de " + DateHelper.getMonthText(dates.startdate) + " de " +
          DateHelper.getYear(dates.startdate) + ", a las " + $filter('myTime')(dates.starttime);
        break;
      case "1111":
        var codeTwo = codeTwoDates(dates.startdate, dates.enddate);
        switch (codeTwo) {
          case "000":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) + " a las " +
              $filter('myTime')(dates.starttime) + " de " + DateHelper.getYear(dates.startdate) + " hasta el " + DateHelper.getDay(dates.enddate) +
              " de " + DateHelper.getMonthText(dates.enddate) + " a las " + $filter('myTime')(dates.endtime) +
              " de " + DateHelper.getYear(dates.enddate);
            break;
          case "001":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) +
              " a las " + $filter('myTime')(dates.starttime)  + " hasta el " +  DateHelper.getDay(dates.enddate) + " de " +
              DateHelper.getMonthText(dates.enddate) + " a las " + $filter('myTime')(dates.endtime) + " de " + DateHelper.getYear(dates.enddate);
            break;
          case "010":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) + " a las " +
              $filter('myTime')(dates.starttime) + " de " + DateHelper.getYear(dates.startdate) + " hasta el " + DateHelper.getDay(dates.enddate) +
              " de " + DateHelper.getMonthText(dates.enddate) + " a las " + $filter('myTime')(dates.endtime) +
              " de " + DateHelper.getYear(dates.enddate);
            break;
          case "011":
            result = "Del " + DateHelper.getDay(dates.startdate) + " al " + DateHelper.getDay(dates.enddate) + " de " +
              DateHelper.getMonthText(dates.startdate) + " de " + DateHelper.getYear(dates.enddate);
            break;
          case "100":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) + " a las " +
              $filter('myTime')(dates.starttime) + " de " + DateHelper.getYear(dates.startdate) + " hasta el " + DateHelper.getDay(dates.enddate) +
              " de " + DateHelper.getMonthText(dates.enddate) + " a las " + $filter('myTime')(dates.endtime) +
              " de " + DateHelper.getYear(dates.enddate);
            break;
          case "101":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) + " al " +
              DateHelper.getDay(dates.enddate) + " de " + DateHelper.getMonthText(dates.enddate) + " a las " + $filter('myTime')(dates.endtime) +
              " de " + DateHelper.getYear(dates.enddate);
            break;
          case "110":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) + " a las " +
              $filter('myTime')(dates.starttime) + " de " + DateHelper.getYear(dates.startdate) + " hasta el " + DateHelper.getDay(dates.enddate) +
              " de " + DateHelper.getMonthText(dates.enddate) + " a las " + $filter('myTime')(dates.endtime) +
              " de " + DateHelper.getYear(dates.enddate);
            break;
          case "111":
            result = "El " + DateHelper.getDay(dates.startdate) +" de " + DateHelper.getMonthText(dates.startdate) + " de " +
              DateHelper.getYear(dates.startdate) + ", de " + $filter('myTime')(dates.starttime) + " a " + $filter('myTime')(dates.endtime);
            break;
        }
        break;
      case "1110":
        var codeTwo = codeTwoDates(dates.startdate, dates.enddate);
        switch (codeTwo) {
          case "000":
            result = "Del " + DateHelper.getDay(dates.startdate) +" de " + DateHelper.getMonthText(dates.startdate) + " de " +
              DateHelper.getYear(dates.startdate) + ", a partir de las " + $filter('myTime')(dates.starttime) +
              " hasta el " + DateHelper.getDay(dates.enddate) +" de " + DateHelper.getMonthText(dates.enddate) + " de " +
              DateHelper.getYear(dates.enddate);
            break;
          case "001":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) +
              " a las " + $filter('myTime')(dates.starttime)  + " hasta el " +  DateHelper.getDay(dates.enddate) + " de " +
              DateHelper.getMonthText(dates.enddate) + " de " + DateHelper.getYear(dates.enddate);
            break;
          case "010":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) +
              " a las " + $filter('myTime')(dates.starttime)  + " hasta el " +  DateHelper.getDay(dates.enddate) + " de " +
              DateHelper.getMonthText(dates.enddate) + " de " + DateHelper.getYear(dates.enddate);
            break;
          case "011":
            result = "Del " + DateHelper.getDay(dates.startdate) + " al " + DateHelper.getDay(dates.enddate) + " de " +
              DateHelper.getMonthText(dates.startdate) + " de " + DateHelper.getYear(dates.enddate);
            break;
          case "100":
            result = "Del " + DateHelper.getDay(dates.startdate) + " al " + DateHelper.getDay(dates.enddate) + " de " +
              DateHelper.getMonthText(dates.startdate) + " de " + DateHelper.getYear(dates.enddate);
            break;
          case "101":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) +
              " a las " + $filter('myTime')(dates.starttime)  + " hasta el " +  DateHelper.getDay(dates.enddate) + " de " +
              DateHelper.getMonthText(dates.enddate) + " de " + DateHelper.getYear(dates.enddate);
            break;
          case "110":
            result = "El " + DateHelper.getDay(dates.startdate) +" de " + DateHelper.getMonthText(dates.startdate) + " de " +
              DateHelper.getYear(dates.startdate) + ", a partir de las " + $filter('myTime')(dates.starttime);
            break;
          case "111":
            result = "El " + DateHelper.getDay(dates.startdate) +" de " + DateHelper.getMonthText(dates.startdate) +
              " de " + DateHelper.getYear(dates.startdate) + " a partir de las " + $filter('myTime')(dates.starttime);
            break;
        }
        break;
      case "1010":
        var codeTwo = codeTwoDates(dates.startdate, dates.enddate);
        switch (codeTwo) {
          case "000":
            result = "Del " + DateHelper.getDay(dates.startdate) +" de " + DateHelper.getMonthText(dates.startdate) + " de " +
              DateHelper.getYear(dates.startdate) + " hasta el " + DateHelper.getDay(dates.enddate) +" de " + DateHelper.getMonthText(dates.enddate) + " de " +
              DateHelper.getYear(dates.enddate);
            break;
          case "001":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) + " al " +
              DateHelper.getDay(dates.enddate) + " de " + DateHelper.getMonthText(dates.enddate) + " de " + DateHelper.getYear(dates.enddate);
            break;
          case "010":
            result = "Del " + DateHelper.getDay(dates.startdate) + " al " + DateHelper.getDay(dates.enddate) + " de " +
              DateHelper.getMonthText(dates.enddate) + " de " + DateHelper.getYear(dates.enddate);
            break;
          case "011":
            result = "Del " + DateHelper.getDay(dates.startdate) + " al " + DateHelper.getDay(dates.enddate) + " de " +
              DateHelper.getMonthText(dates.startdate) + " de " + DateHelper.getYear(dates.enddate);
            break;
          case "100":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) + " al " +
              DateHelper.getDay(dates.enddate) + " de " + DateHelper.getMonthText(dates.enddate)  + " de " + DateHelper.getYear(dates.enddate);
            break;
          case "101":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) + " al " +
              DateHelper.getDay(dates.starttime) + " de " + DateHelper.getMonthText(dates.enddate) + " a las " + $filter('myTime')(dates.endtime) +
              " de " + DateHelper.getYear(dates.enddate);
            break;
          case "110":
            result = "El " + DateHelper.getDay(dates.startdate) +" de " + DateHelper.getMonthText(dates.startdate) + " de " +
              DateHelper.getYear(dates.startdate);
            break;
          case "111":
            result = "El " + DateHelper.getDay(dates.startdate) +" de " + DateHelper.getMonthText(dates.startdate) + " de " +
              DateHelper.getYear(dates.startdate);
            break;
        }
        break;
      case "1011":
        var codeTwo = codeTwoDates(dates.startdate, dates.enddate);
        switch (codeTwo) {
          case "000":
            result = "Del " + DateHelper.getDay(dates.startdate) +" de " + DateHelper.getMonthText(dates.startdate) + " de " +
              DateHelper.getYear(dates.startdate) + " hasta el " + DateHelper.getDay(dates.enddate) +" de " + DateHelper.getMonthText(dates.enddate) +
              " a las " + $filter('myTime')(dates.endtime) + " de " + DateHelper.getYear(dates.enddate);
            break;
          case "001":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) + " al " +
              DateHelper.getDay(dates.enddate) + " de " + DateHelper.getMonthText(dates.enddate) + " a las " + $filter('myTime')(dates.endtime) +
              " de " + DateHelper.getYear(dates.enddate);
            break;
          case "010":
            result = "Del " + DateHelper.getDay(dates.startdate) + " al " + DateHelper.getDay(dates.enddate) + " de " +
              DateHelper.getMonthText(dates.enddate)  + " a las " + $filter('myTime')(dates.endtime) + " de " + DateHelper.getYear(dates.enddate);
            break;
          case "110":
            result = "El " + DateHelper.getDay(dates.startdate) +" de " + DateHelper.getMonthText(dates.startdate) + " de " +
              DateHelper.getYear(dates.startdate);
            break;
          case "100":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) + " al " +
              DateHelper.getDay(dates.enddate) + " de " + DateHelper.getMonthText(dates.enddate) + " a las " + $filter('myTime')(dates.endtime) +
              " de " + DateHelper.getYear(dates.enddate);
            break;
          case "101":
            result = "Del " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) + " al " +
              DateHelper.getDay(dates.enddate) + " de " + DateHelper.getMonthText(dates.enddate) + " a las " + $filter('myTime')(dates.endtime) +
              " de " + DateHelper.getYear(dates.enddate);
            break;
          case "011":
            result = "Del " + DateHelper.getDay(dates.startdate) + " al " + DateHelper.getDay(dates.enddate) + " de " +
              DateHelper.getMonthText(dates.startdate) + " de " + DateHelper.getYear(dates.enddate);
            break;
          case "111":
            result = "El " + DateHelper.getDay(dates.startdate) + " de " + DateHelper.getMonthText(dates.startdate) + " de " +
              " de " + DateHelper.getYear(dates.enddate) + " hasta las " + $filter('myTime')(dates.endtime);
            break;
        }
    }

    return result;
  }

  return function(dates) {
    if (!dates) return;
    return buildDates(dates, codeDates(dates));
  }

})
