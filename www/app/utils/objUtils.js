app.factory('ObjUtils', function() {
  this.addAttribute = function(obj, attrName, value) {
    if (obj != null && obj != undefined) {
      obj[attrName] = value;
    }
  }

  this.arrayContainValue = function(array, property, value) {
    var result = false;
    for (var j = 0; j < array.length; j++) {
      if (normalize(array[j].name.toLowerCase()) == normalize(value.toLowerCase())) {
        return true;
      }
      else {
        if (j == array.length - 1) return false;
      }
    }
    return result;
  }

  this.parseFloat = function(obj, prop) {

    function removeCharsExtrain(str){
      var result = "";
      for (var i = 0; i < str.length; i++) {
        if (str.charAt(i) == '.' || parseInt(str.charAt(i))) {
          result += str.charAt(i);
        }
      }
      return result;
    }

    if (obj[prop] == "") {
      obj[prop] = null;
    } else {
      var reg = new RegExp("(^[0-9]+$)|(^[0-9]+\\.[0-9]{0,2}$)");   //allow number only Ex 11  11.34 11.1 etc
      if (!reg.test(obj[prop])) {
        var input = removeCharsExtrain(obj[prop]);
        if (input == "")
          obj[prop] = null;
        else
          obj[prop] = input;
      }
    }
    console.log(input);
  }

  this.updateArrayProperty = function(array, property, value) {
    for (var j = 0; j < array.length; j++) {
      if (!array[j].hasOwnProperty(property)) {
        console.error("propiedad incorrecta");
        break;
      }
      else {
        array[j][property] = value;
      }
    }
  }

  return this;
})
