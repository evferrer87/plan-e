app.factory("DTOAssembler", function($injector) {

  this.toModel = function (DTO, className) {
    return $injector.get(className)['build'](DTO);
  }

  this.toDTO = function (data, dtoName) {
    return $injector.get(dtoName)['build'](data);
  }

  return this;
});
