app.factory('SelectHelper', function($filter) {

  var self = this;

  self.SelectionArray = function(elements) {

    var items = [];

    if (elements == null || elements == 'undefined') console.error("SelectionArray is null or not defined.");
    for (var i = 0; i < elements.length; i++) {
      elements[i]['selected'] = false;
      items.push(elements[i]);
    }

    return{
      init: function(objs) {
        this.removeAll();
        for (var i = 0; i < objs.length; i++) {
          this.add(objs[i]);
        }
      },
      get: function () {
        return items;
      },
      selectAll: function(value) {
        for (var i = 0; i < items.length; i++) {
          if (items[i].hasOwnProperty('selected')) {
            items[i].selected = value;
          } else {
            console.error('Not all item have the \'selected\' attribute');
          }
        }
      },
      add: function(item) {
        item['selected'] = false;
        items.push(item);
      },
      getIndexById: function(id) {
        var index = -1;
        for (var i = 0; i < items.length; i++) {
          index++;
          if (items[i].id == id) {
            return index;
          }
        }
        return index;
      },
      search: function(id) {
        return $filter('findById')(items, id);
      },
      remove: function(id) {
        var index = this.getIndexById(id);
        if (index != -1) {
          items.splice(index, 1);
        }
      },
      removeAll: function() {
        items.splice(0,items.length);
      },
      anySelected: function() {
        if (items.length == 0) return false;
        for (var i = 0; i < items.length; i++) {
          if (!items[i].hasOwnProperty('selected')) {
            console.error("There is an object that has no selected property");
          } else {
            if (items[i].selected == true) {
              return true;
            }
          }
        }
        return false;
      },
      getAllSelected: function() {
        var result = [];
        for (var i = 0; i < items.length; i++) {
          if (!items[i].hasOwnProperty('selected')) {
            console.error("There is an object that has no selected property");
          } else {
            if (items[i].selected == true) {
              result.push(items[i]);
            }
          }
        }
        return result;
      }

    }
    return SelectionArray;
  }

  return self;
});
