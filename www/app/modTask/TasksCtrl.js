taskModule.controller('TasksCtrl', function($scope, TasksService, $stateParams, $ionicModal, SelectHelper){

  TasksService.getTasks($stateParams.eventId).then(function(tasks) {
    $scope.taskArray = new SelectHelper.SelectionArray(tasks);
  })


  $scope.updateTask = function(task, e) {
    e.stopPropagation();
    task.completed = !task.completed;
    TasksService.saveTask(task, $scope.event.id).then(function(){

    });
  }

  $scope.openEditTaskModal = function(task) {
    $scope.task = task;
    $scope.editTask = angular.copy(task);
    $ionicModal.fromTemplateUrl('app/modTask/EditTask/editTask.html', {
      scope: $scope,
      focusFirstInput: false,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.editTaskModal = modal;
      $scope.editTaskModal.show();
    });

  };

  $scope.closeEditTaskModal = function() {
    $scope.editTaskModal.hide().then(function() {
      $scope.editTaskModal.remove();
    });
  };

  $scope.openAddTaskModal = function() {
    $ionicModal.fromTemplateUrl('app/modTask/AddTask/addTask.html', {
      scope: $scope,
      focusFirstInput: false,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.addTaskModal = modal;
      $scope.addTaskModal.show();
    });
  };

  $scope.closeAddTaskModal = function() {
    $scope.addTaskModal.hide();
  };

  $scope.$on('$destroy', function() {
    if ($scope.editTaskModal) {
      $scope.editTaskModal.remove();
    }

    if ($scope.addTaskModal) {
      $scope.addTaskModal.remove();
    }
  });

});
