taskModule.controller("AddTaskCtrl", function($scope, $ionicModal, TasksService, Task) {

  $scope.saveButtonOptions = {show: false};
  $scope.oldTtask = Task.init();
  $scope.task = Task.init();

  $scope.changeTask = function(){
    if(!angular.equals($scope.oldTtask,$scope.task)){
      $scope.saveButtonOptions.show = true;
    }else{
      $scope.saveButtonOptions.show = false;
    }
  }

  $scope.addTask = function(addTaskForm) {
    if (!addTaskForm.$valid) return;
    TasksService.addTask($scope.event.id, $scope.task).then(function(){
      $scope.saveButtonOptions.show = false;
      $scope.event.tasks_amount++;
      $scope.taskArray.add($scope.task);
      $scope.closeAddTaskModal();
    });
  }
});
