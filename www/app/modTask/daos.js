taskModule.service('TaskDAO', function($q, DB, DateConverter, DTOAssembler) {
  this.getAll = function(eventId) {
    var deferred = $q.defer();
    var sql = "SELECT * FROM tasks WHERE tasks.event_id = ?";
    DB.query(sql, [eventId]).then(function(data){
      var result = [];
      var rows = data.rows;
      for (var i = 0; i < rows.length; i++) {
        var row =  rows.item(i);
        var taskDTO = DTOAssembler.toDTO(row,"TaskDTO");
        taskDTO.startdate = DateConverter.toDateFromTimestamp(row.startdate);
        taskDTO.starttime = DateConverter.toDateFromTimestamp(row.starttime);
        taskDTO.enddate = DateConverter.toDateFromTimestamp(row.enddate);
        taskDTO.endtime = DateConverter.toDateFromTimestamp(row.endtime);
        row.completed == "true" ? taskDTO.completed = true : taskDTO.completed = false;  //ORM return "true" or "false" strings
        result.push(taskDTO);
      }
      deferred.resolve(result);
    }, function(error){
      deferred.reject(error);
    });
    return deferred.promise;
  }

  this.insert = function(eventId, taskDTO) {
    var deferred = $q.defer();
    var task = DTOAssembler.toModel(taskDTO, 'Task');
    task.event_id = eventId;
    DB.insert('tasks', task).then(function(result) {
      deferred.resolve(result.insertId);
    }, function(error){
      deferred.reject({message: error});
    });
    return deferred.promise;
  }

  this.update = function(taskDTO, eventId) {
    var deferred = $q.defer();
    var task = DTOAssembler.toModel(taskDTO, 'Task');
    DB.update('tasks', task).then(function(result) {
      deferred.resolve(result);
    }, function(error) {
      deferred.reject(error);
    });

    return deferred.promise;
  }

  this.remove = function(id) {
    var deferred = $q.defer();
    DB.remove('tasks', id).then(function() {
      deferred.resolve();
    }, function(error){
      deferred.reject(error);
    });
    return deferred.promise;
  };
});
