taskModule.controller("EditTaskCtrl", function($scope, $ionicModal, TasksService, Task) {

  $scope.saveButtonOptions = {show: false};

  $scope.changeTask = function(){
    if(!angular.equals($scope.task, $scope.editTask)){
      $scope.saveButtonOptions.show = true;
    }else{
      $scope.saveButtonOptions.show = false;
    }
  }

  $scope.updateTask = function(editTaskForm) {
    if (!editTaskForm.$valid) return;
    TasksService.saveTask($scope.editTask, $scope.event.id).then(function(){
      angular.copy($scope.editTask,$scope.taskArray.search($scope.editTask.id));
      $scope.saveButtonOptions.show = false;
      $scope.closeEditTaskModal();
    });
  }
});
