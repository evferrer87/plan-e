taskModule.factory("TaskDTO", function() {
  function TaskDTO(id, name, startdate, starttime, enddate, endtime, detail, completed) {
    this.id = id;
    this.name = name;
    this.startdate = startdate;
    this.starttime = starttime;
    this.enddate = enddate;
    this.endtime = endtime;
    this.detail = detail;
    this.completed = completed;
  };

  TaskDTO.build = function(data) {
    return new TaskDTO(
      data.id,
      data.name,
      data.startdate,
      data.starttime,
      data.enddate,
      data.endtime,
      data.detail,
      data.completed
    );
  };

  TaskDTO.init = function() {
    return new TaskDTO(null, undefined, null, null , null, null, false, "");
  };

  return TaskDTO;
});
