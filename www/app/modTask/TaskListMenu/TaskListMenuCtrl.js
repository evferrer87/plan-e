taskModule.controller('TasklistMenuCtrl', function($scope, TasksService) {
  $scope.remove = function(){
    TasksService.removeTask($scope.task.id).then(function() {
      $scope.event.tasks_amount--;
      $scope.taskArray.remove($scope.task.id);
      $scope.popover.hide();
    });
  }
});
