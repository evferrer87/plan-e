taskModule.service('TasksService', function($q, $rootScope, $injector, TaskDAO){

  this.getTasks = function(eventId) {
    var deferred = $q.defer();
    TaskDAO.getAll(eventId).then(function(tasks) {
      deferred.resolve(tasks);
    });
    return deferred.promise;
  }

  this.saveTask = function(task, eventId) {
    var deferred = $q.defer();
    TaskDAO.update(task, eventId).then(function(){
      deferred.resolve();
    },function(error){
      console.log(error);
    });
    return deferred.promise;
  }

  this.removeTask = function(id) {
    var deferred = $q.defer();
    TaskDAO.remove(id).then(function(){
      deferred.resolve();
    },function(error){
      console.log(error);
    });
    return deferred.promise;
  }

  this.addTask = function(eventId, task) {
    var deferred = $q.defer();
    TaskDAO.insert(eventId, task).then(function(id){
      deferred.resolve(task);
    },function(error){
      console.log(error);
    });
    return deferred.promise;
  }

});
