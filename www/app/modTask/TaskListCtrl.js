taskModule.controller('TaskListCtrl', function($scope, $rootScope, $ionicPopover, TasksService){
  $ionicPopover.fromTemplateUrl('app/modTask/TaskListMenu/taskListMenu.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.showSubmenu = function($event, task) {
    $scope.task = task;
    $scope.popover.show($event);
  };
});
