angular.module('SqLite', [])

  .factory('DB', function ($q, DB_CONFIG, $rootScope) {
    var self = this;
    self.db = null;

    self.openDB = function() {
      try {
        if (window.cordova && window.sqlitePlugin) {
          //self.dropDB();
          self.db = window.sqlitePlugin.openDatabase({name: DB_CONFIG.name, location: DB_CONFIG.location});
          self.enableForeignKeySupport();
        } else {
          self.db = window.openDatabase(DB_CONFIG.name, DB_CONFIG.version, DB_CONFIG.description, -1);
        }
      } catch (error) {
        console.log(error);
      }
    }

    self.dropDB = function() {
      self.db = window.sqlitePlugin.deleteDatabase({name: DB_CONFIG.name, location: DB_CONFIG.location});
    }

    self.init = function() {
      self.openDB();
      self.createTables().then(function(){
        /*self.insertAll('events', [
          {
            name: "Evento 4",
            budget: 23,
            startdate: new Date('2016-04-10 02:00').toISOString(),
            starttime: new Date('2016-04-12 03:00').toISOString(),
            enddate: new Date('2016-04-10 02:00').toISOString(),
            endtime: new Date('2016-04-10 04:00').toISOString(),
            detail: "Detalle",
            state: 1
          }
        ], true).then(function(result){
        },function(error){
          console.error(error);
        });

        self.insertAll('locations', [
          {
            placeid: "ChIJL6wn6oAOZ0gRoHExl6nHAAo",
            formatted_address: 23,
            map_zoom: 17,
            event_id: 1,
            latitude: 28.1235459,
            longitude: -15.436257399999931
          }
        ], true).then(function(result){
        },function(error){
          console.error(error);
        });

        self.insertAll('tasks', [
          {
            name: "Task 1",
            startdate: new Date('2016-04-10 02:00').toISOString(),
            starttime: new Date('2016-04-10 04:00').toISOString(),
            enddate: new Date('2016-04-14 02:00').toISOString(),
            endtime: new Date('2016-04-10 05:00').toISOString(),
            completed: false,
            detail: "Siiii",
            event_id: 1},
          {
            name: "Task 2",
            startdate: new Date('2016-04-09 02:00').toISOString(),
            starttime: new Date('2016-04-10 04:00').toISOString(),
            enddate: new Date('2016-04-10 02:00').toISOString(),
            endtime: new Date('2016-04-10 06:00').toISOString(),
            completed: false,
            detail: "Siiii",
            event_id: 1},
          {
            name: "Task 3",
            startdate: new Date('2016-04-09 02:00').toISOString(),
            starttime: new Date('2016-04-10 04:00').toISOString(),
            enddate: new Date('2016-04-10 02:00').toISOString(),
            endtime: new Date('2016-04-10 06:00').toISOString(),
            completed: false, detail: "Siiii",
            event_id: 1
          },
        ], true).then(function(result){
        },function(error){
          console.error(error);
        });

        self.insertAll('guests', [
          { name: "Guest 1", phone: "646466262", email: "sss@gmail.com", event_id: 1},
          { name: "Guest 2", phone: "646466262", email: "sss@gmail.com", event_id: 1},
          { name: "Guest 3", phone: "646466262", email: "sss@gmail.com", event_id: 1},
        ], true).then(function(result){
        },function(error){
          console.error(error);
        });*/

        $rootScope.$broadcast('localDatabaseReady');
      });
    }

    self.enableForeignKeySupport = function() {
      self.db.executeSql("PRAGMA foreign_keys = ON;",[],function(result){
        console.log("Foreign key support activated.");
      });
    }

    self.createTables = function() {
      var deferred = $q.defer();
      var sqls = [];
      var sqlsExecuted = 0;
      self.db.transaction(function (transaction) {
        for (var tableName in DB_CONFIG.tables) {
          var columnsDef = [];
          var columns = DB_CONFIG.tables[tableName]['columns'];
          for (var columnName in columns) {
            var type = columns[columnName];
            columnsDef.push(columnName + ' ' + type);
          }
          var constraintsDef = [];
          var constraints = DB_CONFIG.tables[tableName]['constraints'];
          var constraintsString = '';
          angular.forEach(constraints, function(value, key){
            constraintsDef.push(value);
          });
          if(constraints && Object.keys(constraints).length > 0) {
            constraintsString = ', ' + constraintsDef.join(', ');
          }

          sqls.push({sql: 'CREATE TABLE IF NOT EXISTS ' + tableName + ' (' + columnsDef.join(', ') +
          constraintsString + ')', binding: null});

          var indexes = DB_CONFIG.tables[tableName]['indexes'];
          if (indexes) {
            for (var i = 0; i < Object.keys(indexes).length; i++) {
              sqls.push({sql: 'CREATE ' + indexes[i]});
              console.log(indexes[i]);
            }
          }
        }

        for (var i = 0; i < sqls.length; i++) {
          transaction.executeSql(sqls[i].sql, [], success, error);
          console.log(sqls[i].sql);
        }
      });

      function success (tx, result) {
        console.log("Table created");
        verifySqlsExecuted();
      };

      function error (tx, error) {
        console.log("ERROR: " + error.message);
        verifySqlsExecuted();
      };

      function verifySqlsExecuted(){
        sqlsExecuted++;
        if (sqlsExecuted == sqls.length) {
          deferred.resolve();
        }
      }

      return deferred.promise;
    };

    self.query = function(sql, bindings){
      bindings = typeof bindings !== 'undefined' ? bindings : [];
      var deferred = $q.defer();

      if (self.db == null){
        $rootScope.$on('localDatabaseReady', function(){
          execute();
        });
      }else {
        execute();
      }

      function execute() {
        self.db.transaction(function (transaction) {
          console.log(sql);
          console.log("bindings: " + bindings);
          transaction.executeSql(sql, bindings, function (transaction, result) {

            console.log("bindings: " + bindings);
            //console.log(result.rows.item(0));
            deferred.resolve(result);
          }, function (transaction, error) {
            deferred.reject(error);
          });
        });
      }

      return deferred.promise;
    }

    self.fetchAll = function (result) {
      var output = [];
      for (var i = 0; i < result.rows.length; i++) {
        output.push(result.rows.item(i));
      }
      return output;
    };

    self.fetchFirst = function (result) {
      return result.rows.item(0);
    };

    buildColBindVal = function(tableName, obj) {
      var columns = [], bindings = [], values = [];
      var allColumns = Object.keys(obj);
      for (var i = 0; i < allColumns.length; i++) {
        var col = allColumns[i];
        columns.push(col);
        bindings.push('?');
        values.push(obj[col]);
      }
      return [columns, bindings, values];
    };

    self.buildInsertSQL = function(tableName, obj) {
      var entityObjs = self.buildEntityObjs(tableName, obj, false);
      var columnsBindings = buildColBindVal(tableName, entityObjs[tableName]);
      var columns = columnsBindings[0], bindings = columnsBindings[1], values = columnsBindings[2];

      var sql = 'INSERT INTO ' + tableName + ' (' + columns.join(', ') + ') VALUES ('+ bindings.join(', ') + ')';

      console.log(sql);
      console.log(values);
      return { sql: sql, values: values };
    }

    self.insert = function(tableName, obj) {
      var deferred = $q.defer();
      var sqlInsert = self.buildInsertSQL(tableName, obj);
      console.log(sqlInsert);
      self.query(sqlInsert.sql, sqlInsert.values).then(function(result) {
        deferred.resolve(result);
      }, function(reason) {
        console.log(reason);
        deferred.reject(reason);
      });
      return deferred.promise;
    }

    self.insertAll = function(tableName, data, isTx) {
      var deferred = $q.defer();
      var insertedAmount = 0;
      self.db.transaction(function (transaction) {
        for (var i = 0; i < data.length; i++) {
          var sqlInsert = self.buildInsertSQL(tableName, data[i]);
          transaction.executeSql(sqlInsert.sql, sqlInsert.values, success, error);
        }
      });

      function success (tx, result) {
        data[insertedAmount++]['id'] = result.insertId;
        if (insertedAmount == data.length) {
          console.log("All inserted");
          deferred.resolve(data);
        }
      };

      function error (tx, error) {
        deferred.reject({'mensaje': "ERROR: " + error.message, 'obj':data[insertedAmount]});
        if (isTx == true) {
          return true;
        }
      };

      return deferred.promise;
    };

    self.updateAll = function(tableName, data, isTx) {
      var deferred = $q.defer();
      var updatedAmount = 0;
      self.db.transaction(function (transaction) {
        for (var i = 0; i < data.length; i++) {
          var sqlUpdate = self.buildUpdateQuery(tableName, data[i]);
          console.log(sqlUpdate.sql);
          console.log(sqlUpdate.values);
          transaction.executeSql(sqlUpdate.sql, sqlUpdate.values, success, error);
        }
      });

      function success (tx, result) {
        updatedAmount++;
        if (updatedAmount == data.length) {
          console.log("All updated");
          deferred.resolve(data);
        }
      };

      function error (tx, error) {
        deferred.reject({'mensaje': "ERROR: " + error.message, 'obj':data[updatedAmount]});
        if (isTx == true) {
          return true;
        }
      };

      return deferred.promise;
    };

    self.insertAndUpdateAll = function(tableName, data, isTx) {
      var deferred = $q.defer();
      var dataProcessedAmount = 0;
      var stm = "";
      self.db.transaction(function (transaction) {
        var a = 1;
        for (var i = 0; i < data.length; i++) {
          stm = self.buildInsertSQL(tableName, data[i]);

          if (data[i].id != null && data[i].id != 'undefined'){
            var stm = self.buildUpdateQuery(tableName, data[i]);
          }
          transaction.executeSql(stm.sql, stm.values, success, error);
        }
      });

      function success (tx, result) {
        if (result.hasOwnProperty('insertId')) {
          data[dataProcessedAmount] = result.insertId;
        }
        dataProcessedAmount++;
        if (dataProcessedAmount == data.length) {
          console.log("All data processed");
          deferred.resolve(data);
        }
      };

      function error (tx, error) {
        console.error(stm.sql);
        console.error(stm.values);
        deferred.reject({'mensaje': "ERROR: " + error.message, 'obj':data[dataProcessedAmount]});
        if (isTx == true) {
          return true;
        }
      };

      return deferred.promise;
    };

    self.remove = function(tableName, id) {
      var deferred = $q.defer();
      var bindings = [id];
      var sql = "DELETE FROM " + tableName + " WHERE id = ?";
      self.query(sql, bindings).then(function(result) {
        deferred.resolve(result);
      }, function(reason) {
        console.log(reason);
        deferred.reject(reason);
      });
      return deferred.promise;
    }

    self.removeAll = function(tableName, ids) {
      var deferred = $q.defer();
      var processedAmount = 0;
      var stm = "";
      self.db.transaction(function (transaction) {
        var a = 1;
        for (var i = 0; i < ids.length; i++) {
          stm = "DELETE FROM " + tableName + " WHERE id = ?";
          transaction.executeSql(stm.sql, [ids[i]], success, error);
        }
      });

      function success (tx, result) {
        processedAmount++;
        if (processedAmount == ids.length) {
          console.log("All deleted");
          deferred.resolve();
        }
      };

      function error (tx, error) {
        console.error(stm.sql);
        console.error(stm.values);
        deferred.reject({'mensaje': "ERROR: " + error.message + " id: " + ids[processedAmount]});
        if (isTx == true) {
          return true;
        }
      };

      return deferred.promise;
    }


    self.buildEntityObjs = function(tableName, obj, deep) {
      var entities = {};
      var entity = {};
      var allColumns = Object.keys(DB_CONFIG.tables[tableName]['columns']);
      for (var i = 0; i < allColumns.length; i++) {
        var col = allColumns[i];

        if (RegExp("_id$").test(col)) {
          obj[col] ? entity[col] = obj[col] : entity[col] = null;
          var objectKey = col.replace('_id','');
          if (obj[objectKey] && typeof  obj[objectKey] == "object" && deep) {
            entities[objectKey + "s"] = buildEntityObjs(objectKey + "s", obj[objectKey]);
          }
        }else {
          if (Object.keys(obj).indexOf(col) > -1) {
            entity[col] = obj[col];
          }
        }
      }

      if (deep) {
        var allObjKeys = Object.keys(obj);
        for (var i = 0; i < allColumns.length; i++) {
          var key = allObjKeys[i];
          if (typeof obj[key] == "object") {
            if (DB_CONFIG.tables[key + "s"] && key + "s" != tableName) {
              obj[key][tableName.replace('s', '')+"_id"] = obj[key].id;
              entities[key + "s"] = obj[key];
            }
          }
        }
      }

      entities[tableName] = entity;
      return entities;
    };

    self.buildUpdateQuery = function(tableName, obj) {
      var columnsBindings = buildColBindVal(tableName, obj);
      var columns = columnsBindings[0], bindings = columnsBindings[1], values = columnsBindings[2];

      var indexId = columns.indexOf("id");
      if (indexId > -1) {
        columns.splice(indexId , 1);
        bindings.splice(indexId, 1);
        values.splice(indexId, 1);
      }

      var fieldValue = [];
      for (var i = 0; i < columns.length; i++) {
        fieldValue.push(columns[i] + "=" + bindings[i]);
      }
      var sql = "UPDATE " + tableName + " SET " + fieldValue.join(', ') + " WHERE "+tableName+".id = ?";
      values.push(obj.id);
      return {sql: sql, values: values};
    }

    self.update = function(tableName, obj) {
      var deferred = $q.defer();
      var queryUpdate = self.buildUpdateQuery(tableName, obj);

      self.query(queryUpdate.sql, queryUpdate.values).then(function(result) {
        deferred.resolve(result);
      }, function(reason) {
        console.log(reason);
        deferred.reject(reason);
      });
      return deferred.promise;
    }

    return self;
  });
